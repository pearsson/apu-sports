<?php

use Illuminate\Database\Seeder;
use App\User;

class PermissionRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<=30; $i++){
            DB::table('role_has_permissions')->insert([
                ['role_id' => 1,    'permission_id' =>  $i],
            ]);
        }
    }
}
