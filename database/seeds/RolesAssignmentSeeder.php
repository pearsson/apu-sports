<?php

use Illuminate\Database\Seeder;
use App\User;

class RolesAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i=1; $i<=20; $i++){
            DB::table('model_has_roles')->insert([
                ['role_id' => 1,    'model_type' => 'APP\\User','model_id' => $i]
            ]);
        }

        /*
        User::find(1)->assignRole('Administrator');
        User::find(2)->assignRole('Administrator');
        User::find(3)->assignRole('Member');
        User::find(4)->assignRole('Member');

        User::find(5)->assignRole('Administrator','Member');
        User::find(6)->assignRole('Member');
        User::find(7)->assignRole('Member');
        User::find(8)->assignRole('Member');
        User::find(9)->assignRole('Member');
        User::find(10)->assignRole('Member');
        User::find(11)->assignRole('Member');
        User::find(12)->assignRole('Member');
        User::find(13)->assignRole('Member');
        User::find(14)->assignRole('Member');
        User::find(15)->assignRole('Member');
        User::find(16)->assignRole('Member');
        User::find(17)->assignRole('Member');
        User::find(18)->assignRole('Member');
        User::find(19)->assignRole('Member');
        User::find(20)->assignRole('Member');
        */
    }
}
