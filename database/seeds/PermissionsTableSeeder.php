<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert(['name' => 'edit image', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'add team', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit team', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete team', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'edit user role', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit user status', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add member', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit member', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete member', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add league', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit league', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete league', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add player', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit player', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete player', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add season', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit season', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete season', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add team', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit team', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete team', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add event', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit event', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete event', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add result', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit result', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete result', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add game', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit game', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete game', 'guard_name' => 'web']);

        DB::table('permissions')->insert(['name' => 'add match', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'edit match', 'guard_name' => 'web']);
        DB::table('permissions')->insert(['name' => 'delete match', 'guard_name' => 'web']);

    }
}
