<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
	        'name' => 'Administrator',
            'guard_name' => 'web'
        ]);
            
        DB::table('roles')->insert([
	        'name' => 'Member',
            'guard_name' => 'web'
	    ]);
    }
}
