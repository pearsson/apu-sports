<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,20) as $index) {            
	        DB::table('users')->insert([
                'name' => $faker->name,
                'email'  => $faker->email,
                'password' => bcrypt('secret'),
                'height' => $faker->numberBetween(100,200),
                'weight' => $faker->numberBetween(40,100),
                'birthdate' =>  Carbon::createFromFormat('m/d/Y', $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('m/d/Y')),
                'phone' => $faker->phoneNumber,
                'photo' => $faker->imageUrl($width = 640, $height = 480, 'cats'),
                'description' => $faker->realText($maxNbChars = 200, $indexSize = 2)
            ]);
	    }
    }
}
