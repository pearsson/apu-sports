<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'photo', 'description'
    ];

    public function teams(){
        return $this->hasMany('App\Team');
    }

    public function upcomingMatches(){
        return $this->hasManyThrough('App\Match', 'App\League');
    }

    public function leagues(){
        return $this->hasMany('App\League');
    }
}
