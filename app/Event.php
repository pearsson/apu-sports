<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [
        'name', 'type', 'venue','start_at','end_at','photo','description'
    ];

    protected $dates = [
        'start_at','end_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_at' => 'datetime:m/d/Y',
        'end_at' => 'datetime:m/d/Y'
    ];

    public function match()
    {
        return $this->hasOne('App\Match');
    }

    public function photos()
    {
        return $this->hasMany('App\EventPhoto');
    }
}
