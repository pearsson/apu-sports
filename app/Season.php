<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $table = 'seasons';

    protected $fillable = [
        'start_at','end_at','league_id'
    ];

    protected $dates = [
        'start_at','end_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_at' => 'datetime:m/d/Y',
        'end_at' => 'datetime:m/d/Y'
    ];

    public function league(){
        return $this->belongsTo('App\League');
    }   

    public function Matches()
    {
        return $this->hasMany('App\Match');
    }


}
