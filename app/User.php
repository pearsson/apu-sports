<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','height','weight','birthdate','phone','photo','contract_start','contract_end','position','number','description'
    ];

    protected $dates = [
        'birthdate',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthdate' => 'datetime:m/d/Y'
    ];

    public function getAgeAttribute(){
        //return Carbon::parse($this->birthdate)->age;
        $this->birthdate->age;
    }

    public function team(){
        return $this->belongsTo('App\Team');
    }

}
