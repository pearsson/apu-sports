<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    protected $fillable = [
        'birthdate', 'name', 'home_studium','home_city','photo','game_id','description'
    ];

    protected $dates = [
        'birthdate',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birthdate' => 'datetime:m/d/Y'
    ];

    public function players()
    {
        return $this->hasMany('App\Player');
    }

    public function game(){
        return $this->belongsTo('App\Game');
    }   

    public function homeMatches()
    {
        return $this->hasMany('App\Match', 'team_a');
    }

    public function awayMatches()
    {
        return $this->hasMany('App\Match', 'team_b');
    }

    public function getMatchesAttribute()
    {
        return collect($this->awayMatches,$this->homeMatches);
    }
}
