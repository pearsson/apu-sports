<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends User
{
    protected $table = 'players';

    protected $fillable = [
        'user_id', 'team_id', 'contract_start', 'contract_end', 'description', 'position', 'number', 'photo' 
    ];

    protected $dates = [
        'contract_start', 'contract_end'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contract_start' => 'datetime:m/d/Y',
        'contract_end' => 'datetime:m/d/Y'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }   

}