<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Player;
use App\User;
use App\Team;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PlayerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(Player::all());
    }

    public function show($id)
    {
        return response()->json(Player::find($id));
    }

    public function store(Request $request)
    {
        $validation = $request->validate([           
            'user' => 'required|integer|min:1',
            'team' => 'required|integer|min:1',
            'contract_start' => 'date_format:"m/d/Y"|required',
            'contract_end' => 'date_format:"m/d/Y"|required',
            'description' => 'required|nullable',

            'position' => 'required|string|max:100',
            'number' => 'required|numeric',

            'photo' => 'required|file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try {
            $image_file = $request->file('photo');
                $base_path = 'images/players/' . date('Y') . '/' . date('m') . '/';

                    if (! \File::exists($base_path)) {
                        \File::makeDirectory($base_path, 0777, true);
                    }

                $new_file_name = time().$image_file->getClientOriginalName();

                $image_file_path = $base_path.$new_file_name;

                $upload_image = $image_file->move($base_path, $new_file_name);

                if (!$upload_image) throw new \Exception('Failed to upload image');

                $user = User::findOrFail($request->get('user'));
                $team = Team::findOrFail($request->get('team'));

                $player =  Player::create([
                    'user_id' => $request->get('user'),
                    'team_id' => $request->get('team'),
                    'contract_start' => Carbon::createFromFormat('m/d/Y', $request->get('contract_start')),
                    'contract_end' => Carbon::createFromFormat('m/d/Y', $request->get('contract_end')),
                    'description' => $request->get('description'),
                    'position' => $request->get('position'),
                    'number' => $request->get('number'),
                    'photo' => $image_file_path        
                ]);

            return response()->json($player, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {
        $validation = $request->validate([           
            'player' => 'required|integer|min:1',
            'contact_start' => 'date_format:"m/d/Y"|required',
            'contact_end' => 'date_format:"m/d/Y"|required',
            'description' => 'required|nullable',

            'position' => 'required|number|max:100',
            'number' => 'required|numeric',

            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try{
            $player = Player::findOrFail($id);

            $player =  $player->update([
                'contact_start' => Carbon::createFromFormat('m/d/Y', $request->get('contact_start')),
                'contact_end' => Carbon::createFromFormat('m/d/Y', $request->get('contact_end')),
                'description' => $request->get('description'),
                'position' => $request->get('position'),
                'number' => $request->get('number'),
                'photo' => $image_file_path        
            ]);

            return response()->json($player, 200);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        Player::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
