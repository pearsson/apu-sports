<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Team;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(Team::all());
    }

    public function show($id)
    {
        $team = Team::with(['players.user','game','homeMatches.results','homeMatches.events','homeMatches.league','awayMatches.results','awayMatches.events','awayMatches.league'])->find($id);
        
        return response()->json($team,200);
    }

    public function store(Request $request)
    {
        $validation = $request->validate([           
            'name' => 'required|string|max:255',
            'game' => 'required|integer|min:0',
            'home_studium' => 'required|string|max:200',
            'home_city' => 'sometimes|nullable|string|max:200',
            'description' => 'required|string',
            'birthdate' => 'required|date_format:"m/d/Y"',
            'photo' => 'required|file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try {
            $image_file = $request->file('photo');

                $base_path = 'images/teams/' . date('Y') . '/' . date('m') . '/';

                if (! \File::exists($base_path)) {
                    \File::makeDirectory($base_path, 0777, true);
                }

                $new_file_name = time().$image_file->getClientOriginalName();

                $image_file_path = $base_path.$new_file_name;

                $upload_image = $image_file->move($base_path, $new_file_name);

                if (!$upload_image) {
                    throw new \Exception('Failed to upload image');
                }

                $team =  Team::create([
                    'name' => $request->name,
                    'game_id' => $request->game,
                    'home_studium' => $request->home_studium,
                    'home_city' => $request->home_city,
                    'description' => $request->description,
                    'birthdate' =>  Carbon::createFromFormat('m/d/Y', $request->get('birthdate')),
                    'photo' => $image_file_path,            
                ]);

            return response()->json($team, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {
        $validation = $request->validate([           
            //'name' => 'required|string|max:255',
            'game' => 'required|integer|min:0',
            'home_studium' => 'required|string|max:200',
            'home_city' => 'sometimes|nullable|string|max:200',
            'description' => 'nullable|string',
            'birthdate' => 'required|date_format:"m/d/Y"',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try{
            $team = Team::findOrFail($id);
            $team->update([
                'game_id' => $request->game,
                'description' => $request->description,
                'birthdate' =>  Carbon::createFromFormat('m/d/Y', $request->get('birthdate')),
                'home_studium' => $request->home_studium,
                ]);

            return response()->json($team, 200);
        }catch(\Exception $e){
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        Team::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
