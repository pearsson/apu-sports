<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\League;
use App\Game;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class LeagueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(League::with(['seasons'])->get());
    }

    public function show($id)
    {
        return response()->json(League::with(['game','matches.result','matches.teamA','matches.teamB','matches.results','teams','seasons'])->find($id));
    }

    public function store(Request $request)
    {
        $validation = $request->validate([           
            'name' => 'required|string|max:255',
            'game' => 'required|integer',
            'description' => 'required|string',
            'photo' => 'required|file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try {
            $game = Game::findOrFail($request->game);
            $image_file = $request->file('photo');

                $base_path = 'images/leagues/' . date('Y') . '/' . date('m') . '/';

                    if (! \File::exists($base_path)) {
                        \File::makeDirectory($base_path, 0777, true);
                    }

                $new_file_name = time().$image_file->getClientOriginalName();

                $image_file_path = $base_path.$new_file_name;

                $upload_image = $image_file->move($base_path, $new_file_name);

                if (!$upload_image) {
                    throw new \Exception('Failed to upload image');
                }

                $league =  League::create([
                    'name' => $request->get('name'),
                    'game_id' => $request->get('game'),
                    'description' => $request->get('description'),
                    'photo' => $image_file_path,            
                ]);

                $league->game();

            return response()->json($league, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {
        $validation = $request->validate([           
            'name' => 'required|string|max:255',
            'game' => 'required|integer',
            'description' => 'required|nullable|string',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try{
            $league = League::findOrFail($id);
            $league->update($request->all());

            $league->update([
                'name' => $request->name,
                'game_id' => $request->game,
                'description' => $request->description,
                ]);

            return response()->json($league, 200);
        }catch(\Exception $e){
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        League::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function tables(){
        $season_table = DB::select('select league_tables.team_id, teams.name, league_tables.points, league_tables.wins - league_tables.losses as GoalsDifference, league_tables.wins, league_tables.losses from league_tables inner join teams on league_tables.team_id = teams.id order by league_tables.points;');
    }
}
