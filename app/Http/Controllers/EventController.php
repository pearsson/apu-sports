<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Controllers\Controller;
use App\League;
use App\Match;
use App\Season;
use App\Team;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    function list() {
        return response()->json(Event::with(['match.results', 'match.season', 'match.league'])->get());
    }

    public function show($id)
    {
        return response()->json(Event::with(['match.teamB', 'match.teamA', 'match.results', 'match.season', 'match.league', 'photos'])->find($id));
    }

    public function store(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required|string|max:255',
            'type' => 'in:social,tour,match',
            'venue' => 'required|string|max:255',
            'start_at' => 'date_format:"m/d/Y"|required',
            'end_at' => 'date_format:"m/d/Y"|required',
            'description' => 'nullable|string',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280',

            'league' => 'sometimes|nullable|integer|min:0',
            'season' => 'sometimes|nullable|integer|min:0',
            'team_a' => 'sometimes|nullable|integer|min:0',
            'team_b' => 'sometimes|nullable|integer|min:0',

        ]);

        try {

            DB::beginTRansaction();
            $image_file = $request->file('photo');

            $base_path = 'images/events/' . date('Y') . '/' . date('m') . '/';

            if (!\File::exists($base_path)) {
                \File::makeDirectory($base_path, 0777, true);
            }

            $new_file_name = time() . $image_file->getClientOriginalName();

            $image_file_path = $base_path . $new_file_name;

            $upload_image = $image_file->move($base_path, $new_file_name);

            if (!$upload_image) {
                throw new \Exception('Failed to upload image');
            }

            $event = Event::create([
                'name' => $request->get('name'),
                'type' => $request->get('type'),
                'venue' => $request->get('venue'),
                'start_at' => Carbon::createFromFormat('m/d/Y', $request->get('start_at')),
                'end_at' => Carbon::createFromFormat('m/d/Y', $request->get('end_at')),
                'description' => $request->get('description'),
                'photo' => $image_file_path,
            ]);

            if ($request->league) {
                $league = League::findOrFail($request->league);
                $season = Season::findOrFail($request->season);
                $teamA = Team::findOrFail($request->get('team_a'));
                $teamB = Team::findOrFail($request->get('team_b'));

                $match = Match::create([
                    'league_id' => $league->id,
                    'season_id' => $season->id,
                    'event_id' => $event->id,
                    'team_a' => $teamA->id,
                    'team_b' => $teamB->id,
                ]);
            }

            $event->match->season;

            DB::commit();

            return response()->json($event, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['errors' => ["error" => $e->getMessage()]], 501);
        }
    }

    public function update($id, Request $request)
    {

        $validation = $request->validate([
            'name' => 'required|string|max:255',
            'type' => 'in:social,tour,match',
            'venue' => 'required|string|max:255',
            'start_at' => 'date_format:"m/d/Y"|required',
            'end_at' => 'date_format:"m/d/Y"|required',
            'description' => 'nullable|string',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280',
        ]);

        try {
            $event = Event::findOrFail($id);

            $event = $event->update([
                'type' => $request->get('type'),
                'venue' => $request->get('venue'),
                'start_at' => Carbon::createFromFormat('m/d/Y', $request->get('start_at')),
                'end_at' => Carbon::createFromFormat('m/d/Y', $request->get('end_at')),
                'description' => $request->get('description'),
                'photo' => $image_file_path,
            ]);

            return response()->json($event, 201);
        } catch (\Exception $e) {
            return response()->json(['errors' => ["error" => $e->getMessage()]], 501);
        }
    }

    public function delete($id)
    {
        Event::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function uploadImages(Request $request)
    {
        $validation = $request->validate([
            'photo' => 'required',
            'photo.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'event' => 'required|integer|min:0',
        ]);

        try {

            DB::beginTransaction();

            $event = Event::findOrFail($request->event);

            if ($request->hasFile('photo')) {
                $images = $request->file('photo');

                $base_path = 'images/events/' . date('Y') . '/' . date('m') . '/';

                if (!\File::exists($base_path)) {
                    \File::makeDirectory($base_path, 0777, true);
                }

                foreach ($images as $key => $image) {
                    $image_file = $image;

                    $new_file_name = time() . $image_file->getClientOriginalName();

                    $image_file_path = $base_path . $new_file_name;

                    $upload_image = $image_file->move($base_path, $new_file_name);

                    if (!$upload_image) {
                        throw new \Exception('Failed to upload image');
                    }

                    $photo = \App\EventPhoto::create([
                        'event_id' => $event->id,
                        'photo' => $image_file_path,
                    ]);

                }
            }

            $event->photos;

            DB::commit();

            return response()->json($event, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['errors' => ["error" => $e->getMessage()]], 501);
        }
    }
}
