<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Season;
use App\League;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SeasonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(Season::with(['league','matches.results'])->get());
    }

    public function show($id)
    {
        $season_table = DB::select('select league_tables.team_id, teams.name, league_tables.points, league_tables.wins - league_tables.losses as GoalsDifference, league_tables.wins, league_tables.losses from league_tables inner join teams on league_tables.team_id = teams.id where league_tables.season_id = '.$id.' order by league_tables.points;');

        $season = Season::with(['league','matches.results'])->find($id);

        $data = array(
            'season'=>$season,
            'season_table'=>$season_table
        );

        return response()->json($data,200);
    }

    public function store(Request $request)
    {
        $validation = $request->validate([           
            'league' => 'required|integer|min:0',
            'start_at' => 'required|date_format:"m/d/Y"',
            'end_at' => 'required|date_format:"m/d/Y"',
        ]);

        try {
            $league = League::findOrFail($request->league);
            $futureSeasons = $league->whereHas('seasons', function ($query) use($request) {
                            $query->whereDate('end_at', '>=', Carbon::createFromFormat('m/d/Y', $request->start_at)->toDateString());
                        })->count();

            if($futureSeasons){
                throw new \Exception('A league can not have overlapping seasons');
            }
            $season =  Season::create([
                'league_id' => $request->get('league'),
                'start_at' => Carbon::createFromFormat('m/d/Y', $request->get('start_at')),
                'end_at' => Carbon::createFromFormat('m/d/Y', $request->get('end_at')),  
            ]);

            $season->league;

            return response()->json($season, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {
        $validation = $request->validate([           
            'start_at' => 'required|date_format:"m/d/Y"',
            'end_at' => 'required|date_format:"m/d/Y"',
        ]);

        try {
            $season = Season::findOrFail($id);

            $futureSeasons = League::where('id',$season->league_id)->whereHas('seasons', function ($query) use($request){
                $query->whereDate('end_at', '>=', Carbon::createFromFormat('m/d/Y', $request->start_at)->toDateString());
            })->count();

            if($futureSeasons){
                throw new \Exception('A league can not have overlapping seasons');
            }
                $season =  $season->update([
                    'start_at' => Carbon::createFromFormat('m/d/Y', $request->get('start_at')),
                    'end_at' => Carbon::createFromFormat('m/d/Y', $request->get('end_at')),  
                ]);

                $season->league;

            return response()->json($season, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        Season::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
