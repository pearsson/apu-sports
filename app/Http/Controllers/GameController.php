<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Game;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(Game::with(['teams','leagues','upcomingMatches'])->get());
    }

    public function show($id)
    {
        return response()->json(Game::with(['teams','leagues','upcomingMatches.event','upcomingMatches.teamA','upcomingMatches.teamB','upcomingMatches.league'])->find($id));
    }

    public function store(Request $request)
    {
        $validation = $request->validate([           
            'name' => 'required|string|max:255|unique:games',
            'description' => 'required|string',
            'photo' => 'required|file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

         try {
            $image_file = $request->file('photo');

                $base_path = 'images/games/';

                if (! \File::exists($base_path)) {
                    \File::makeDirectory($base_path, 0777, true);
                }

                $new_file_name = time().$image_file->getClientOriginalName();

                $image_file_path = $base_path.$new_file_name;

                $upload_image = $image_file->move($base_path, $new_file_name);

                if (!$upload_image) {
                    throw new \Exception('Failed to upload image');
                }

                $game =  Game::create([
                    'name' => $request->get('name'),
                    'description' => $request->get('description'),
                    'photo' => $image_file_path,            
                ]);

                return response()->json($game, 201);
            
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => ['required','string'],
            'photo' => ['file','mimes:jpg,jpeg,png','dimensions:max_width=1280,max_height=1280'], 
            'description' => ['nullable','string'],        
        ];

        $messages = [
            'mimes'    => 'The :attribute must be .jpg, .jpeg or .png image.',
        ];

         $validator = Validator::make($request->all(), $rules,$messages);
         $validator->validate();

        try{
            DB::beginTransaction();
            $game = Game::findOrFail($id);

            $game->update([
                'name' => $request->name,
                'description' => $request->description,
                ]);
            DB::commit();
            return response()->json($game, 200);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        try{
            $game = Game::findOrFail($id)->delete();
            return response()->json($game,200);
        }catch(\Exception $e){
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }
}
