<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class MatchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(User::all());
    }

    public function show($id)
    {
        return response()->json(User::find($id));
    }

    public function create(Request $request)
    {
        $validation = $request->validate([           
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'height' => 'required|numeric',
            'weight' => 'required|numeric',
            'birthdate' => 'date_format:"m/d/Y"|required',
            'phone' => 'required|numeric','unique:users',
            'password_confirmation' => 'required',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
        ]);

        try {
            $image_file = $request->file('logo');

            if($image_file){
                $base_path = 'images/users/' . date('Y') . '/' . date('m') . '/';

                    if (! \File::exists($base_path)) {
                        \File::makeDirectory($base_path, 0777, true);
                    }

                $new_file_name = time().$image_file->getClientOriginalName();

                $image_file_path = $base_path.$new_file_name;

                $upload_image = $image_file->move($base_path, $new_file_name);

                if (!$upload_image) {
                    throw new \Exception('Failed to upload image');
                }
            }

                $user =  User::create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                    'height' => $request->get('height'),
                    'weight' => $request->get('weight'),
                    'birthdate' => Carbon::createFromFormat('m/d/Y', $request->get('birthdate')),
                    'phone' => $request->get('phone'),
                    'photo' => $image_file_path,            
                ]);

            return response()->json($user, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {
        try{
            $user = User::findOrFail($id);
            $user->update($request->all());

            return response()->json($user, 200);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
