<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;
use CArbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'height' => 'required|numeric',
            'weight' => 'required|numeric',
            'birthdate' => 'date_format:"m/d/Y"|required',
            'phone' => 'required|numeric','unique:users',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
            'description' => 'nullable|string',
        
        ];

        $messages = [
            'mimes'    => 'The :attribute must be .jpg, .jpeg or .png image.',
            'date_format'    => 'The :attribute must have mmonth/day/Year format eg.08/23/2019.',
        ];

        return Validator::make($data, $rules,$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request = request();

        $profileImage = $request->file('photo');

        $image_file_path = '';

        if($profileImage){
            $base_path = 'images/users/';

            try {
                if (! \File::exists($base_path)) {
                    \File::makeDirectory($base_path, 0777, true);
                }
            } catch (\Exception $e) {
                // exception of creating a folder
                return response()->json(['message'=>'Failed to create directory'],501);
            }

            $new_file_name = time().$profileImage->getClientOriginalName();

            $image_file_path = $base_path.$new_file_name;

            $upload_image = $profileImage->move($base_path, $new_file_name);

            if (!$upload_image) {
                abort(501);
            }
        }
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'height' => $data['height'],
            'weight' => $data['weight'],
            'birthdate' => Carbon::createFromFormat('m/d/Y', $data['birthdate']),
            'phone' => $data['phone'],
            'photo' => $image_file_path, 
            'description' => $data['description'], 
        ]);
    }
}
