<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return response()->json(User::all());
    }

    public function show($id)
    {
        return response()->json(User::find($id));
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'height' => 'required|numeric',
            'weight' => 'required|numeric',
            'birthdate' => 'date_format:"m/d/Y"|required',
            'phone' => 'required|numeric','unique:users',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
            'description' => 'nullable|string',        
        ];

        $messages = [
            'mimes'    => 'The :attribute must be .jpg, .jpeg or .png image.',
            'date_format'    => 'The :attribute must have mmonth/day/Year format eg.08/23/2019.',
        ];

        $validator = Validator::make($request->all(), $rules,$messages);
        if ($validator->fails()) {
            return response()->json(["errors"=>$validator->errors()->toArray()],422);
        }


        try {
            $image_file = $request->file('photo');

                $base_path = 'images/users/';

                    if (! \File::exists($base_path)) {
                        \File::makeDirectory($base_path, 0777, true);
                    }


                $new_file_name = time().$image_file->getClientOriginalName();

                $image_file_path = $base_path.$new_file_name;

                $upload_image = $image_file->move($base_path, $new_file_name);

                if (!$upload_image) {
                    throw new \Exception('Failed to upload image');
                }

                $user =  User::create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                    'height' => $request->get('height'),
                    'weight' => $request->get('weight'),
                    'birthdate' => Carbon::createFromFormat('m/d/Y', $request->get('birthdate')),
                    'phone' => $request->get('phone'),
                    'photo' => $image_file_path,            
                ]);

            return response()->json($user, 201);
        } catch (\Exception $e) {
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function update($id, Request $request)
    {        
        $rules = [
            'old_password' => ['required_with:new_password','string'],
            'new_password' => ['bail','nullable','min:6', 'string'],
            'height' => 'required|numeric',
            'weight' => 'required|numeric',
            'birthdate' => 'date_format:"m/d/Y"|required',
            'phone' => 'required|numeric','unique:users',
            'photo' => 'file|mimes:jpg,jpeg,png|dimensions:max_width=1280,max_height=1280', 
            'description' => 'nullable|string',        
        ];

        $messages = [
            'mimes'    => 'The :attribute must be .jpg, .jpeg or .png image.',
            'date_format'    => 'The :attribute must have mmonth/day/Year format eg.08/23/2019.',
        ];

        $validator = Validator::make($request->all(), $rules,$messages);
        if ($validator->fails()) {
            return response()->json(["errors"=>$validator->errors()->toArray()],422);
        }

        try{
            DB::beginTransaction();
            $user = User::findOrFail($id);
            
            if (Hash::check($request->old_password, $user->password)) { 
                $user->update(['password' => Hash::make($request->old_password)]);
            }else{
                throw new \Exception('Wrong old password');
            }

            $user->update([
                'height' => $request->height,
                'weight' => $request->weight,
                'birthdate' => Carbon::createFromFormat('m/d/Y', $request->birthdate),
                'phone' => $request->phone,
                'description' => $request->description,
                ]);
            DB::commit();
            return response()->json($user, 200);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }

    public function delete($id)
    {
        try{
            $user = User::findOrFail($id);
            if($user->id == Auth::user()->id)
                throw new \Exception('Currently loged in user can not be deleted');
            
            $user->delete();
            return response('Deleted Successfully', 200);
        }catch(\Exception $e){
            return response()->json(['errors'=>["error"=>$e->getMessage()]],501);
        }
    }
}
