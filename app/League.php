<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{

    protected $table = 'leagues';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'photo', 'description','game_id'
    ];

    public function game(){
        return $this->belongsTo('App\Game');
    }

    public function matches(){
        return $this->hasMany('App\Match');
    }

    public function teams(){
        return $this->belongsToMany('App\Team', 'league_teams', 'league_id', 'team_id');
    }

    public function seasons(){
        return $this->hasMany('App\Season');
    }
}
