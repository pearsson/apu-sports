<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{

    protected $table = 'matches';

    protected $fillable = [
        'league_id', 'season_id', 'event_id','team_a','team_b'
    ];

    public function results(){
        return $this->hasOne('App\Result');
    }

    public function league(){
        return $this->belongsTo('App\League');
    }

    public function season(){
        return $this->belongsTo('App\Season');
    }

    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function teamA(){
        return $this->belongsTo('App\Team', 'team_a');
    }

    public function teamB(){
        return $this->belongsTo('App\Team', 'team_b');
    }
}
