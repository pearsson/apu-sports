/*global module:false*/
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        handlebars: {
            all: {
                files: {
                    "public/js/templates.js": ["resources/js/components/*.handlebars"]
                }
            }
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-handlebars');

};
