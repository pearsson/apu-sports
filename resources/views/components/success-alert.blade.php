<div class="bg-green-lightest border-l-4 border-green text-green-dark p-4 w-4/5 mx-auto my-4 alert body-alert success-alert"
    role="alert">
    <p class="font-bold">Success</p>
    <div class="errors"></div>
</div>