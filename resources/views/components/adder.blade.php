<div class="w-4/5 mx-auto my-4 p-3">
    <div class="container">
        <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" id="btn-add-errors"><i class="ti-plus"></i> + add</button>

        <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" id="btn-remove-errors"><i class="ti-plus"></i> - clear</button>
    </div>
</div>