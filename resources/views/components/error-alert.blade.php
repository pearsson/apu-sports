<div class="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4 w-4/5 mx-auto my-4 alert body-alert error-alert"
    role="alert">
    <p class="font-bold">Be Warned</p>
    <div class="errors"></div>
</div>