@if (count($errors) > 0)
<div class="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4 w-4/5 mx-auto my-4" role="alert">
  <p class="font-bold">Be Warned</p>
  <div class="errors">
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
  </div>
</div>
@endif
<div class="w-4/5 mx-auto my-4 p-3" id="login">
  <div class="container">
    <form class="mb-2 pb-2" method="POST" action="{{ route('login') }}">
      @csrf


      <div class="md:flex md:items-center mb-6">
        <div class="md:w-1/3">
          <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4" for="inline-full-name">
                                          Email
                                        </label>
        </div>
        <div class="md:w-2/3">
          <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple"
            id="inline-full-name" type="text" name="email" placeholder="jane.doe@gmail.com">
        </div>
      </div>
      <div class="md:flex md:items-center mb-6">
        <div class="md:w-1/3">
          <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4" for="inline-username">
                                          Password
                                        </label>
        </div>
        <div class="md:w-2/3">
          <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple"
            id="inline-username" type="password" placeholder="************" name="password">
        </div>
      </div>
      <div class="md:flex md:items-center mb-6">
        <div class="md:w-1/3"></div>
        <label class="md:w-2/3 block text-grey font-bold">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember">
                                        <span class="text-sm">
                                                Remember Me
                                        </span>
                                      </label>
      </div>
      <div class="md:flex md:items-center">
        <div class="md:w-1/3"></div>
        <div class="md:w-2/3">
          <button class="shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded submit"
            type="submit">
                                          Login
                                        </button>

          <button class="shadow bg-grey-light hover:bg-grey text-grey-darkest font-bold py-2 px-4 rounded inline-flex items-center"
            type="button" data-micromodal-close aria-label="Close this dialog window">
                                          Cancel
                                        </button>
        </div>
      </div>

    </form>
  </div>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>



@section('custom-scripts') @parent
<script>

</script>
@endsection