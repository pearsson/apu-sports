<div class="modal micromodal-slide" id="login-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container modal-md" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <header class="modal__header">
                <h2 class="modal__title" id="modal-1-title">
                    Login
                </h2>
                <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
            </header>
            <main class="modal__content" id="modal-1-content">
                <div class="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4 alert" role="alert">
                    <p class="font-bold">Be Warned</p>

                </div>
                <form class="mb-2 pb-2">
                    @csrf

                    <div class="m-1">
                        <form class="w-full max-w-xs">
                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4" for="inline-full-name">
                                          Full Name
                                        </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple"
                                        id="inline-full-name" type="text" value="Jane Doe">
                                </div>
                            </div>
                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4" for="inline-username">
                                          Password
                                        </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple"
                                        id="inline-username" type="password" placeholder="******************">
                                </div>
                            </div>
                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3"></div>
                                <label class="md:w-2/3 block text-grey font-bold">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember">
                                        <span class="text-sm">
                                                Remember Me
                                        </span>
                                      </label>
                            </div>
                            <div class="md:flex md:items-center">
                                <div class="md:w-1/3"></div>
                                <div class="md:w-2/3">
                                    <button class="shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded submit"
                                        type="button">
                                          Login
                                        </button>

                                        <button class="shadow bg-grey-light hover:bg-grey text-grey-darkest font-bold py-2 px-4 rounded inline-flex items-center"
                                        type="button"  data-micromodal-close aria-label="Close this dialog window">
                                          Cancel
                                        </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </form>
            </main>
            <footer class="modal__footer">

            </footer>
        </div>
    </div>
</div>


@section('custom-scripts') @parent
<script>
$('document').ready(function() {

  $('#login-modal .submit').on('click', function(){
    console.log('login');
    //login(new FormData($("#login-modal form")[0]),errorFunction,successFunction);
  });

  $('#btn-add-member').on('click', function() {
    MicroModal.show('member-add-modal');
  });
});

</script>
@endsection