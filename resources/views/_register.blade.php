@if (count($errors) > 0)
<div class="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4 w-4/5 mx-auto my-4" role="alert">
    <p class="font-bold">Be Warned</p>
    <div class="errors">
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif
<div class="w-4/5 mx-auto my-4 p-3" id="register">
    <div class="container">
        <form method="POST" action="{{ route('register') }}" class="w-full max-w-md" enctype="multipart/form-data">
            @csrf
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-first-name">
                          Full Name
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        type="text" placeholder="Jane" name="name" value="{{ old('name') }}">
                </div>
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-last-name">
                          Email
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        type="email" placeholder="Doe" name="email" value="{{ old('email') }}">
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-password">
                          Password
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        type="password" placeholder="*****" name="password" value="{{ old('password') }}">
                </div>

                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-password">
                          Confirm Password
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        type="password" placeholder="******************" name="password_confirmation">
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-2">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-city">
                          Birthdate
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey datepicker"
                        type="text" placeholder="m/d/Y" name="birthdate" value="{{ old('birthdate') }}">
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-state">
                          Phone #
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        type="text" placeholder="50" name="phone" value="{{ old('phone') }}">
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-2">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-state">
                          Weight (Kg)
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        type="number" placeholder="50" name="weight" value="{{ old('weight') }}">
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-zip">
                          Height (cm)
                        </label>
                    <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        type="number" placeholder="180" name="height" value="{{ old('height') }}">
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-password">
                              Description
                            </label>
                    <textarea class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                        name="description">{{ old('email') }}</textarea>
                </div>

                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-password">
                              Profile Picture
                            </label>

                    <div class="image-changer-wrapper">
                        <div class="img-overlay">
                            <i class="ti-pencil"></i>
                        </div>
                        <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTU1LjI4IiBoZWlnaHQ9Ijk3LjA1IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA2MjEuMTggMzkwLjYzIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KPG1ldGFkYXRhPgo8cmRmOlJERj4KPGNjOldvcmsgcmRmOmFib3V0PSIiPgo8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KPGRjOnR5cGUgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIvPgo8ZGM6dGl0bGUvPgo8L2NjOldvcms+CjwvcmRmOlJERj4KPC9tZXRhZGF0YT4KPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIuMjQ5MyAtOS4zNzM2KSI+CjxnIHRyYW5zZm9ybT0ibWF0cml4KDEuMDQxNyAwIDAgLjk3NjU2IDAgOS4zNzM2KSI+CjxyZWN0IHg9Ii0xLjI4NzhlLTEyIiB3aWR0aD0iNjAwIiBoZWlnaHQ9IjQwMCIgcnk9IjIuNzI3OSIgZmlsbD0iIzYzNjk2YyIgb3BhY2l0eT0iLjk4Ii8+CjwvZz4KPGcgdHJhbnNmb3JtPSJtYXRyaXgoMS44NzExIDAgMCAxLjY1MjggLTI0Ni4wMyAtMTMwLjc0KSI+CjxwYXRoIGQ9Im0yNDMuMzEgMTU5LjAyYy0zLjI0IDMuNDgtMi4zMSA3LjQ2LTIuMzEgMTEuOTh2MjUgMzdjMC4wMSAyLjU0LTAuMzIgNi40OCAxLjYgOC40IDEuNjMgMS42NCA0LjI2IDEuNTYgNi40IDEuNmgxOCA4MmMyLjE5IDAgNS44NCAwLjIxIDcuNjktMS4wMiAzLjQ4LTIuMzMgMi4zMS0xMy45MSAyLjMxLTE3Ljk4di01NmMtMC4wMS0yLjU0IDAuMzItNi40OC0xLjYtOC40LTEuNjMtMS42NC00LjI2LTEuNTYtNi40LTEuNmgtMTctNjJjLTYuNDggMC0yMy41NS0wLjkzLTI4LjY5IDEuMDJ6bTEwOC42OSA1Ljk4djU4Yy01Ljc3LTIuNzEtMTguNjgtMTUuMDctMjQtMjAuMDItMi4yMS0yLjA3LTYuNjgtNi45OS05Ljk5LTYuNC0zLjA5IDAuNTYtOC43MyA4Ljc2LTExLjAxIDExLjQyLTUuMi0zLjcyLTkuNDctOC41MS0xNC0xMy0xLjU5LTEuNTgtNC44Ny01LjEzLTctNS42Ni0zLjI3LTAuODMtOC42NiA0LjcyLTExIDYuODMtOC4yNyA3LjQ3LTE4LjExIDE3LjY2LTI3IDIzLjgzdi01NWgxMDR6bS0xOSA0LjU3Yy01LjU3IDEuNjktOS45NyA1LjI0LTEwLjU4IDExLjQzLTAuNzkgOC4wOCA3Ljc1IDE1LjI5IDE1LjU4IDEzLjQzIDE0LjgzLTMuNTIgMTEuOC0yNi4yNy01LTI0Ljg2em0yLjAxIDcuMTdjOC44NiAyLjEgNS41NSAxMS4xMyAwIDEwLjcyLTQuOTgtMC4zNi04LjU1LTguMjMgMC0xMC43MnptLTE3LjAxIDI3LjI2YzUuNjUgMi42NyAyMC43MyAxNi45OCAyNiAyMiAzLjc2IDMuNTkgNi40NSA0Ljk3IDggMTBoLTEwNGMwLjA4LTIuMi0wLjAxLTMuOTQgMS4wMi02IDIuMjktNC41OCAyMC4wNS0xOS41MiAyNC45OC0yNC4wOSAyLjQ1LTIuMjcgNy41LTguMDQgMTEtNy44IDMuNjMgMC4yNSAxMi45IDEwLjk2IDE2IDEzLjg1IDIuNjcgMi40OCA1LjIxIDUuMjEgOC44MyAyLjYyIDEuMjktMC45MiAzLjA1LTMuMjggNC4wNC00LjU4IDAgMCA0LjEzLTYgNC4xMy02eiIgZmlsbD0iI2EwYTdhYiIvPgo8L2c+CjwvZz4KPC9zdmc+Cg=="
                        />
                        <input type="file" class="new-img-upload" name="photo" />
                    </div>
                </div>
            </div>
            <div class="md:flex md:items-center">
                <div class="md:w-1/3"></div>
                <div class="md:w-2/3">
                    <button class="shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded submit"
                        type="submit">register</button>
                </div>
            </div>
        </form>
    </div>
</div>