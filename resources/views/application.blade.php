@extends('layouts.base') 
@section('content')
    @include('layouts.navigation')
    @include('components.error-alert')
    @include('components.success-alert')
    @include('home')
    @include('member.list')
    @include('league.list')
    @include('game.list')
    @include('team.list')
    @include('event.list')
@endsection
 
@section('custom-scripts') @parent
<script>
    $(document).ready(function(){
        $('form').submit(function () {
            return false;
        });
        
    });

</script>
@endsection