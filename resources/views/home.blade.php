<div class="section w-4/5 mx-auto my-4 p-3" id="home">
    <div class="container">
        <div class="flex justify-between content-center items-center mb-3">
            <h4>Leagues Tables</h4>

            <div class="flex my-2 items-center">
                <div class="flex flex-wrap -mx-3 mb-2 match-input">
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-city">League</label>
                        <div class="relative">
                            <select name="league" class="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"></select>
                            <div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-grey-darker">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="grid-state">Season</label>
                        <div class="relative">
                            <select name="season" class="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"></select>
                            <div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-grey-darker">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex content-center mb-3 flex-wrap" id="league-table-listing">
            <table class="w-full text-left flex-wrap" style="border-collapse:collapse">
                <thead>
                    <tr>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">#</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">Teams</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">W</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">P</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">D</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">L</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">I</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">I</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">M</span>
                        </th>
                        <th class="py-3 bg-grey-lighter uppercase text-sm text-grey border-b border-grey-light">
                            <span class="box1">PTS</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="my-2">
                        <td class="p-1 leading-loose">1</td>
                        <td class="p-1 leading-loose">Army Club</td>
                        <td class="p-1 leading-loose">40</td>
                        <td class="p-1 leading-loose">30</td>
                        <td class="p-1 leading-loose">20</td>
                        <td class="p-1 leading-loose">15</td>
                        <td class="p-1 leading-loose">10</td>
                        <td class="p-1 leading-loose">5</td>
                        <td class="p-1 leading-loose">2</td>
                        <td class="p-1 leading-loose">3</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Zeshi Club</td>
                        <td>150</td>
                        <td>100</td>
                        <td>90</td>
                        <td>80</td>
                        <td>70</td>
                        <td>60</td>
                        <td>50</td>
                        <td>190</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nation</td>
                        <td>10</td>
                        <td>20</td>
                        <td>30</td>
                        <td>40</td>
                        <td>50</td>
                        <td>60</td>
                        <td>70</td>
                        <td>80</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>National</td>
                        <td>20</td>
                        <td>30</td>
                        <td>40</td>
                        <td>50</td>
                        <td>60</td>
                        <td>70</td>
                        <td>80</td>
                        <td>90</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Brazil</td>
                        <td>30</td>
                        <td>40</td>
                        <td>50</td>
                        <td>60</td>
                        <td>70</td>
                        <td>80</td>
                        <td>90</td>
                        <td>100</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Bnki</td>
                        <td>40</td>
                        <td>50</td>
                        <td>60</td>
                        <td>70</td>
                        <td>80</td>
                        <td>90</td>
                        <td>100</td>
                        <td>110</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Ribon</td>
                        <td>50</td>
                        <td>60</td>
                        <td>70</td>
                        <td>80</td>
                        <td>90</td>
                        <td>100</td>
                        <td>110</td>
                        <td>120</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Ligeria</td>
                        <td>60</td>
                        <td>70</td>
                        <td>80</td>
                        <td>90</td>
                        <td>100</td>
                        <td>110</td>
                        <td>120</td>
                        <td>130</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Bravo</td>
                        <td>70</td>
                        <td>80</td>
                        <td>90</td>
                        <td>110</td>
                        <td>120</td>
                        <td>130</td>
                        <td>140</td>
                        <td>150</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Limaia</td>
                        <td>80</td>
                        <td>90</td>
                        <td>100</td>
                        <td>110</td>
                        <td>120</td>
                        <td>130</td>
                        <td>140</td>
                        <td>150</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Nomli</td>
                        <td>100</td>
                        <td>110</td>
                        <td>120</td>
                        <td>130</td>
                        <td>140</td>
                        <td>150</td>
                        <td>160</td>
                        <td>170</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>Lebia</td>
                        <td>130</td>
                        <td>140</td>
                        <td>150</td>
                        <td>160</td>
                        <td>170</td>
                        <td>180</td>
                        <td>190</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Leighm</td>
                        <td>140</td>
                        <td>150</td>
                        <td>160</td>
                        <td>170</td>
                        <td>180</td>
                        <td>190</td>
                        <td>200</td>
                        <td>210</td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Potra</td>
                        <td>150</td>
                        <td>160</td>
                        <td>170</td>
                        <td>180</td>
                        <td>190</td>
                        <td>200</td>
                        <td>210</td>
                        <td>220</td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Opter</td>
                        <td>170</td>
                        <td>180</td>
                        <td>190</td>
                        <td>200</td>
                        <td>210</td>
                        <td>220</td>
                        <td>230</td>
                        <td>240</td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>Utry</td>
                        <td>180</td>
                        <td>190</td>
                        <td>200</td>
                        <td>210</td>
                        <td>220</td>
                        <td>230</td>
                        <td>240</td>
                        <td>250</td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>Uthr Perdaish</td>
                        <td>190</td>
                        <td>200</td>
                        <td>210</td>
                        <td>220</td>
                        <td>230</td>
                        <td>240</td>
                        <td>250</td>
                        <td>260</td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>Lower Pardasih</td>
                        <td>200</td>
                        <td>210</td>
                        <td>230</td>
                        <td>240</td>
                        <td>250</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>Lamia</td>
                        <td>210</td>
                        <td>220</td>
                        <td>230</td>
                        <td>240</td>
                        <td>250</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>Terada</td>
                        <td>220</td>
                        <td>230</td>
                        <td>240</td>
                        <td>250</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                        <td>290</td>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td>Matyu</td>
                        <td>230</td>
                        <td>240</td>
                        <td>250</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                        <td>290</td>
                        <td>300</td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td>Lomti</td>
                        <td>240</td>
                        <td>250</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                        <td>290</td>
                        <td>300</td>
                        <td>310</td>
                    </tr>
                    <tr>
                        <td>23</td>
                        <td>Ltryia</td>
                        <td>250</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                        <td>290</td>
                        <td>300</td>
                        <td>310</td>
                        <td>320</td>
                    </tr>
                    <tr>
                        <td>24</td>
                        <td>Abtract</td>
                        <td>260</td>
                        <td>270</td>
                        <td>280</td>
                        <td>290</td>
                        <td>300</td>
                        <td>310</td>
                        <td>320</td>
                        <td>-</td>
                    </tr>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>

    </div>
</div>