<!-- Start Navigation -->
<nav hidden>
    <div class="nav-header">
        <a href="#" class="brand"> <i class="fa fa-futbol-o" aria-hidden="true"></i> APU Sports</a>
        <button class="toggle-bar"><span class="fa fa-bars"></span></button>
    </div>
    <ul class="menu">
        <li><a href="/#/home">Home</a></li>
        <li><a href="/#/members">Members</a></li>
        <li><a href="/#/leagues">Leagues</a></li>
        <li><a href="/#/games">Games</a></li>
        <li><a href="/#/teams">Teams</a></li>
        <li><a href="/#/events">Events</a></li>
        @guest
        <li><a href="/login">Login</a></li>
        <li><a href="/register">Register</a></li>
        @else
        <li><a href="/#/profile">{{ Auth::user()->name }}</a></li>
        <li><a href="/#/logout">Logout</a></li>
        @endguest
    </ul>
</nav>
<!-- End Navigation -->