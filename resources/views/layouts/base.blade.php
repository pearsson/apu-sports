<!doctype html>
<html>

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'APU Sports') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/tailwind.min.css" />
    <link rel="stylesheet" href="css/coreNavigation-1.1.3.min.css" />
    <link rel="stylesheet" href="css/micromodal.css" />
    <link rel="stylesheet" href="css/coreNavigation-custom.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="css/select2.min.css" />

    <script src="js/director.min.js"></script>

</head>

<body style='display: none'>

    @yield('content')

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/coreNavigation-1.1.3.min.js"></script>
    <script src="js/micromodal.min.js"></script>
    <script src="js/axios.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/imagePicker.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/select2.full.min.js"></script>
    <script src="js/handlebars.runtime.min.js"></script>
    <script src="js/templates.js"></script>

    <script src="js/home.js"></script>
    <script src="js/members.js"></script>
    <script src="js/leagues.js"></script>
    <script src="js/players.js"></script>

    <script src="js/games.js"></script>
    <script src="js/events.js"></script>
    <script src="js/teams.js"></script>
    <script src="js/seasons.js"></script>

    <script src="js/login.js"></script>
    <script src="js/register.js"></script>
    <script src="js/js-search.js"></script>
    <script src="js/search.js"></script>
    <script src="js/global.js"></script>

    @yield('custom-scripts')
    <script>
        $('nav').coreNavigation({
            menuPosition: "right",
            container: true
        });
        $('document').ready(function () {
            axios.defaults.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

            //
            // create some functions to be executed when
            // the correct route is issued by the user.
            //
            $(".datepicker").attr("autocomplete", "off");
            $(".datepicker").datepicker();          
            
            var allroutes = function () {
                var route = window.location.hash.slice(2);
                var sections = $('section');
                var section;

                section = sections.filter( $.escapeSelector( "[data-route=" + route + "]" ));

                if (section.length) {
                    sections.hide(250);
                    section.show(250);
                }
            };

            //
            // define the routing table.
            //
            var routes = {
                "/": showHome,
                "/home": showHome,
                "/members": showMembers,
                "/leagues": showLeagues,
                "/leagues/:leagueId": showLeague,
                "/games": showGames,
                "/games/:gameId": showGame,
                "/teams": showTeams,
                "/teams/:teamId": showTeam,
                "/events": showEvents,
                "/events/:eventId": showEvent,
                "/seasons/:seasonId": showSeason,
                "/logout": function(){
                    logout(new FormData($("#logout-form")[0]));
                },
            };

            //
            // instantiate the router.
            //
            var router = Router(routes);

            //
            // a global configuration setting.
            //
            router.configure({
                on: allroutes
            });


            $('#btn-add-errors').on('click', function(){
                $('.body-alert.success-alert .errors').append($("<p></p>").text(' Successfully created'));
            });
            $('#btn-remove-errors').on('click', function(){
                $('.body-alert.success-alert .errors').empty();
            });

            const alerts = document.querySelectorAll(".alert .errors");

            const mutationConfig = {
                attributes: true,
                childList: true,
                subtree: true,
                characterData: true,
                characterDataOldValue: true
            };

            var onMutate = function (mutationsList) {
                mutationsList.forEach(mutation => {
                    let errors = mutation.target.querySelectorAll(".errors p");
                    if (errors.length) {
                        mutation.target.parentNode.style.display = "block";
                    } else {
                        mutation.target.parentNode.style.display = "none";
                    }
                });
            };

            var observer = new MutationObserver(onMutate);

            Array.prototype.forEach.call(alerts, function (el, i) {
                observer.observe(el, mutationConfig);
            });

            @can('edit member')
                app.members.edit_permission = true;
            @endcan

            @can('delete member')
                app.members.delete_permission = true;
            @endcan

            @can('edit game')
                app.games.edit_permission = true;
            @endcan

            @can('delete game')
                app.games.delete_permission = true;
            @endcan

            @can('edit team')
                app.teams.edit_permission = true;
            @endcan

            @can('delete team')
                app.teams.delete_permission = true;
            @endcan

            @can('edit league')
                app.leagues.edit_permission = true;
            @endcan

            @can('delete league')
                app.leagues.delete_permission = true;
            @endcan

            @can('edit event')
                app.events.edit_permission = true;
            @endcan

            @can('delete event')
                app.events.delete_permission = true;
            @endcan

            @can('edit season')
                app.seasons.edit_permission = true;
            @endcan

            @can('delete season')
                app.seasons.delete_permission = true;
            @endcan

            @can('edit player')
                app.players.edit_permission = true;
            @endcan

            @can('delete player')
                app.players.delete_permission = true;
            @endcan

            @can('edit match')
                app.matches.edit_permission = true;
            @endcan

            @can('delete match')
                app.matches.delete_permission = true;
            @endcan

            new Promise(function(resolve, reject) {

                app.init();
                resolve(true);

                }).then(function(result) {
                    getMembers().then((result)=>{
                        getGames().then((result)=>{
                            getEvents().then((result)=>{
                                getLeagues().then((result)=>{
                                    getTeams().then((result)=>{
                                        getSeasons().then((result)=>{
                                            getPlayers().then((result)=>{
                                                $('body').show();
                                                router.init();      
                                            }); 
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
        });
    </script>

</body>

</html>