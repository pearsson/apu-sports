@extends('layouts.base') 
@section('content')
    @include('layouts.navigation')
    @include('components.error-alert')
    @include('components.success-alert')
    @include('_login')
@endsection
 
@section('custom-scripts')
@endsection