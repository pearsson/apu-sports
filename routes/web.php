<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('application');
});

Route::get('/mess', function () {
    $dp = Auth::user()->getPermissionsViaRoles();
    dd($dp->toArray());
});

Auth::routes();

Route::group(['prefix' => 'members'], function () {
    Route::get('/', ['uses' => 'UserController@list']);
    Route::get('/{id}', ['uses' => 'UserController@show']);
});

Route::group(['prefix' => 'teams'], function () {
    Route::get('/', ['uses' => 'TeamController@list']);
    Route::get('/{id}', ['uses' => 'TeamController@show']);
});

Route::group(['prefix' => 'events'], function () {
    Route::get('/', ['uses' => 'EventController@list']);
    Route::get('/{id}', ['uses' => 'EventController@show']);
});

Route::group(['prefix' => 'games'], function () {
    Route::get('/', ['uses' => 'GameController@list']);
    Route::get('/{id}', ['uses' => 'GameController@show']);
});

Route::group(['prefix' => 'leagues'], function () {
    Route::get('/', ['uses' => 'LeagueController@list']);
    Route::get('/{id}', ['uses' => 'LeagueController@show']);
    Route::get('/tables', ['uses' => 'LeagueController@tables']);
});

Route::group(['prefix' => 'seasons'], function () {
    Route::get('/', ['uses' => 'SeasonController@list']);
    Route::get('/{id}', ['uses' => 'SeasonController@show']);
});

Route::group(['prefix' => 'players'], function () {
    Route::get('/', ['uses' => 'PlayerController@list']);
    Route::get('/{id}', ['uses' => 'PlayerController@show']);
});

Route::group(['prefix' => 'matches'], function () {
    Route::get('/', ['uses' => 'MatchController@list']);
    Route::get('/{id}', ['uses' => 'MatchController@show']);
});

Route::middleware(['auth'])->group(function () {

    Route::get('/home', function () {
        return view('home');
    });

    Route::group(['prefix' => 'members'], function () {
        Route::post('/', ['uses' => 'UserController@store']);
        Route::put('/{id}', ['uses' => 'UserController@update']);
        Route::delete('/{id}', ['uses' => 'UserController@delete']);
    });

    Route::group(['prefix' => 'teams'], function () {
        Route::post('/', ['uses' => 'TeamController@store']);
        Route::put('/{id}', ['uses' => 'TeamController@update']);
        Route::delete('/{id}', ['uses' => 'TeamController@delete']);
    });

    Route::group(['prefix' => 'events'], function () {
        Route::post('/', ['uses' => 'EventController@store']);
        Route::put('/{id}', ['uses' => 'EventController@update']);
        Route::delete('/{id}', ['uses' => 'EventController@delete']);
        Route::put('/{id}/images', ['uses' => 'EventController@uploadImages']);
    });

    Route::group(['prefix' => 'games'], function () {
        Route::post('/', ['uses' => 'GameController@store']);
        Route::put('/{id}', ['uses' => 'GameController@update']);
        Route::delete('/{id}', ['uses' => 'GameController@delete']);
    });

    Route::group(['prefix' => 'leagues'], function () {
        Route::post('/', ['uses' => 'LeagueController@store']);
        Route::put('/{id}', ['uses' => 'LeagueController@update']);
        Route::delete('/{id}', ['uses' => 'LeagueController@delete']);
    });

    Route::group(['prefix' => 'players'], function () {
        Route::post('/', ['uses' => 'PlayerController@store']);
        Route::put('/{id}', ['uses' => 'PlayerController@update']);
        Route::delete('/{id}', ['uses' => 'PlayerController@delete']);
    });

    Route::group(['prefix' => 'seasons'], function () {
        Route::post('/', ['uses' => 'SeasonController@store']);
        Route::put('/{id}', ['uses' => 'SeasonController@update']);
        Route::delete('/{id}', ['uses' => 'SeasonController@delete']);
    });

    Route::group(['prefix' => 'matches'], function () {
        Route::post('/', ['uses' => 'MatchController@store']);
        Route::put('/{id}', ['uses' => 'MatchController@update']);
        Route::delete('/{id}', ['uses' => 'MatchController@delete']);
    });
});
