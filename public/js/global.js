let app = {
    user: {},
    members: {
        members: [],
        searcher: null,
        filtered: Array,
        element: null,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.searcher.addIndex('phone');
            this.searcher.addIndex('email');
            this.filtered = this.members;
            this.searcher.addDocuments(this.members);
        },
        search: function (query) {
            this.filtered = (query) ? this.searcher.search(query) : this.members;
            this.paint();
        },
        set: function (members) {
            this.members = members;
            this.reIndexSearch();
            this.paint();
        },
        add: function (user) {
            this.members.unshift(user);
            this.reIndexSearch();
            this.paint();
        },
        delete: function (id) {
            for (var i = this.members.length; i--;) {
                if (this.members[i].id === parseInt(id)) {
                    this.members.splice(i, 1);
                    this.reIndexSearch();
                    this.paint();
                    break;
                }
            }
        },
        paint: function () {
            this.element.empty();
            permissions = {
                "edit_permission": this.edit_permission,
                "delete_permission": this.delete_permission
            }

            Array.prototype.forEach.call(this.filtered, (member, i) => {
                var outp = Handlebars.templates.user({
                    "user": member,
                    "permissions": permissions
                });
                this.element.append(outp);
            });
        },
        get: function (id) {
            for (var i = this.members.length; i--;) {
                if (this.members[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object

                    let result = JSON.parse(JSON.stringify(this.members[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (member) {
            this.delete(member.id);
            this.add(member);
        },

    },

    leagues: {
        leagues: [],
        searcher: null,
        filtered: Array,
        element: null,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.searcher.addIndex('game.name`');
            this.filtered = this.leagues;
            this.searcher.addDocuments(this.leagues);
        },
        search: function (query) {
            this.filtered = (query) ? this.searcher.search(query) : this.leagues;
            this.paint();
        },
        set: function (leagues) {
            this.leagues = leagues;
            this.reIndexSearch();
            this.paint();
        },
        add: function (league) {
            this.leagues.unshift(league);
            this.reIndexSearch();
            this.paint();
        },
        delete: function (id) {
            for (var i = this.leagues.length; i--;) {
                if (this.leagues[i].id === parseInt(id)) {
                    this.leagues.splice(i, 1);
                    this.reIndexSearch();
                    this.paint();
                    break;
                }
            }
        },
        paint: function () {
            this.element.empty();
            permissions = {
                "edit_permission": this.edit_permission,
                "delete_permission": this.delete_permission
            }
            Array.prototype.forEach.call(this.filtered, (league, i) => {
                var outp = Handlebars.templates.league({
                    "league": league,
                    "permissions": permissions
                });
                this.element.append(outp);
            });
        },
        get: function (id) {
            for (var i = this.leagues.length; i--;) {
                if (this.leagues[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object

                    let result = JSON.parse(JSON.stringify(this.leagues[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (league) {
            this.delete(league.id);
            this.add(league);
        },

    },

    games: {
        games: [],
        searcher: null,
        filtered: Array,
        element: null,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.filtered = this.games;
            this.searcher.addDocuments(this.games);
        },
        search: function (query) {
            this.filtered = (query) ? this.searcher.search(query) : this.games;
            this.paint();
        },
        set: function (games) {
            this.games = games;
            this.reIndexSearch();
            this.paint();
        },
        add: function (game) {
            this.games.unshift(game);
            this.reIndexSearch();
            this.paint();
        },
        delete: function (id) {
            for (var i = this.games.length; i--;) {
                if (this.games[i].id === parseInt(id)) {
                    this.games.splice(i, 1);
                    this.reIndexSearch();
                    this.paint();
                    break;
                }
            }
        },
        paint: function () {
            this.element.empty();
            permissions = {
                "edit_permission": this.edit_permission,
                "delete_permission": this.delete_permission
            }
            Array.prototype.forEach.call(this.filtered, (games, i) => {
                var outp = Handlebars.templates.game({
                    "game": games,
                    "permissions": permissions
                });
                this.element.append(outp);
            });
        },
        get: function (id) {
            for (var i = this.games.length; i--;) {
                if (this.games[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object
                    let result = JSON.parse(JSON.stringify(this.games[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (game) {
            this.delete(game.id);
            this.add(game);
        },

    },

    teams: {
        teams: [],
        searcher: null,
        filtered: Array,
        element: null,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.filtered = this.teams;
            this.searcher.addDocuments(this.teams);
        },
        search: function (query) {
            this.filtered = (query) ? this.searcher.search(query) : this.teams;
            this.paint();
        },
        set: function (teams) {
            this.teams = teams;
            this.reIndexSearch();
            this.paint();
        },
        add: function (team) {
            this.teams.unshift(team);
            this.reIndexSearch();
            this.paint();
        },
        delete: function (id) {
            for (var i = this.teams.length; i--;) {
                if (this.teams[i].id === parseInt(id)) {
                    this.teams.splice(i, 1);
                    this.reIndexSearch();
                    this.paint();
                    break;
                }
            }
        },
        paint: function () {
            this.element.empty();
            permissions = {
                "edit_permission": this.edit_permission,
                "delete_permission": this.delete_permission
            }

            Array.prototype.forEach.call(this.filtered, (teams, i) => {
                var outp = Handlebars.templates.team({
                    "team": teams,
                    "permissions": permissions
                });
                this.element.append(outp);
            });
        },
        get: function (id) {
            for (var i = this.teams.length; i--;) {
                if (this.teams[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object

                    let result = JSON.parse(JSON.stringify(this.teams[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (team) {
            this.delete(team.id);
            this.add(team);
        },

    },

    seasons: {
        seasons: [],
        searcher: null,
        filtered: Array,
        element: null,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.filtered = this.seasons;
            this.searcher.addDocuments(this.seasons);
        },
        search: function (query) {
            this.filtered = (query) ? this.searcher.search(query) : this.seasons;
            this.paint();
        },
        set: function (seasons) {
            this.seasons = seasons;
            this.reIndexSearch();
            this.paint();
        },
        add: function (season) {
            this.seasons.unshift(season);
            this.reIndexSearch();
            this.paint();
        },
        delete: function (id) {
            for (var i = this.seasons.length; i--;) {
                if (this.seasons[i].id === parseInt(id)) {
                    this.seasons.splice(i, 1);
                    this.reIndexSearch();
                    this.paint();
                    break;
                }
            }
        },
        paint: function () {
            this.element.empty();
            permissions = {
                "edit_permission": this.edit_permission,
                "delete_permission": this.delete_permission
            }

            Array.prototype.forEach.call(this.filtered, (seasons, i) => {
                var outp = Handlebars.templates.season({
                    "season": seasons,
                    "permissions": permissions
                });
                this.element.append(outp);
            });
        },
        get: function (id) {
            for (var i = this.seasons.length; i--;) {
                if (this.seasons[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object

                    let result = JSON.parse(JSON.stringify(this.seasons[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (season) {
            this.delete(season.id);
            this.add(season);
        },

    },

    events: {
        events: [],
        searcher: null,
        filtered: Array,
        element: null,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.filtered = this.events;
            this.searcher.addDocuments(this.events);
        },
        search: function (query) {
            this.filtered = (query) ? this.searcher.search(query) : this.events;
            this.paint();
        },
        set: function (events) {
            this.events = events;
            this.reIndexSearch();
            this.paint();
        },
        add: function (event) {
            this.events.unshift(event);
            this.reIndexSearch();
            this.paint();
        },
        delete: function (id) {
            for (var i = this.events.length; i--;) {
                if (this.events[i].id === parseInt(id)) {
                    this.events.splice(i, 1);
                    this.reIndexSearch();
                    this.paint();
                    break;
                }
            }
        },
        paint: function () {
            this.element.empty();
            permissions = {
                "edit_permission": this.edit_permission,
                "delete_permission": this.delete_permission
            }

            Array.prototype.forEach.call(this.filtered, (events, i) => {
                var outp = Handlebars.templates.event({
                    "event": events,
                    "permissions": permissions
                });
                this.element.append(outp);
            });
        },
        get: function (id) {
            for (var i = this.events.length; i--;) {
                if (this.events[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object

                    let result = JSON.parse(JSON.stringify(this.events[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (event) {
            this.delete(event.id);
            this.add(event);
        },

    },
    players: {
        players: [],
        searcher: null,
        filtered: Array,
        edit_permission: false,
        delete_permission: false,

        reIndexSearch: function () {
            this.searcher = new JsSearch.Search('id');
            this.searcher.addIndex('name');
            this.filtered = this.players;
            this.searcher.addDocuments(this.players);
        },
        set: function (players) {
            this.players = players;
            this.reIndexSearch();
        },
        add: function (player) {
            this.players.unshift(player);
            this.reIndexSearch();
        },
        delete: function (id) {
            for (var i = this.players.length; i--;) {
                if (this.players[i].id === parseInt(id)) {
                    this.players.splice(i, 1);
                    this.reIndexSearch();
                    break;
                }
            }
        },
        get: function (id) {
            for (var i = this.players.length; i--;) {
                if (this.players[i].id === parseInt(id)) {
                    //weird /hacky but correct way of deep copying object

                    let result = JSON.parse(JSON.stringify(this.players[i]));
                    return result;
                }
            }
            return null;
        },
        edit: function (player) {
            this.delete(player.id);
            this.add(player);
        },

    },
    init: function () {
        this.members.element = $('#members-listing');
        this.members.reIndexSearch();

        this.leagues.element = $('#leagues-listing');
        this.leagues.reIndexSearch();

        this.games.element = $('#games-listing');
        this.games.reIndexSearch();

        this.teams.element = $('#teams-listing');
        this.teams.reIndexSearch();

        this.seasons.element = $('#seasons-listing');
        this.seasons.reIndexSearch();

        this.events.element = $('#events-listing');
        this.events.reIndexSearch();
    }
};

function successFunction(messages) {
    Array.prototype.forEach.call(messages, function (message, i) {
        $('.body-alert.success-alert .errors').append($("<p></p>").text(message));
    });
};

function errorFunction(errors) {
    Array.prototype.forEach.call(errors, function (error, i) {
        $('.body-alert.error-alert .errors').append($("<p></p>").text(error));
    });
};

let clearAlerts = function () {
    $('.alert .errors').empty();
}
