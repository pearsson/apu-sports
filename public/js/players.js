$("document").ready(function () {

    $("#player-add-modal select[name='type']").on("change", function () {
        let type = $(this).val();
        console.log(type);
        if (type == "match") {
            $("#player-add-modal .match-input").show();
        } else {
            $("#player-add-modal .match-input").hide();
        }
    });

    $("#player-add-modal select[name='league']").on("change", function () {
        $("#player-edit-modal .error-alert .errors").empty();
        let league_id = $(this).val();
        let seasons = app.leagues.get(league_id);
        console.log(seasons);
        if (seasons.length > 0) {
            $('#player-add-modal select[name="season"]').empty();
            $.each(seasons, function (index, season) {
                const option = $('<option>', {
                    value: season.id,
                    text: season.name
                });
                $('#player-add-modal select[name="season"]').append(option);
            });
        } else {
            $("#player-edit-modal .error-alert .errors").append(
                $("<p></p>").text('This league has no season(s)')
            );
        }
    });

    $("#player-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#player-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.players.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("player-add-modal");
        };
        createPlayer(
            new FormData($("#player-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-player").on("click", function () {
        clearAlerts();
        console.log('adding players');

        // Set up the Select2 control

        console.log(app.members.members);

        let selectData = $.map(app.members.members, function (obj) {
            obj.id = obj.id;
            obj.text = obj.name +' | '+obj.email;
            return obj;
          });

        $('#player-add-modal select[name="user"]').select2({
            data:selectData,
            dropdownCss:'debix'
        });

        /*
        // Fetch the preselected item, and add to the control
        var studentSelect = $('#player-add-modal select[name="user"]');
        $.ajax({
            type: 'GET',
            url: '/members' + studentId
        }).then(function (data) {
            // create the option and append to Select2
            var option = new Option(data.full_name, data.id, true, true);
            studentSelect.append(option).trigger('change');

            // manually trigger the `select2:select` event
            studentSelect.trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        });
        
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        let teams = app.teams.teams;
        if (teams.length < 2) {
            errorFunction(['There are less than 2 registered teams']);
            return
        }

        let leagues = app.leagues.leagues;
        if (leagues.length < 1) {
            errorFunction(['No registered sports leagues']);
            return
        }

        let type = $("#player-add-modal select[name='type']").val();
        console.log(type);
        if (type == "match") {
            $("#player-add-modal .match-input").show();
        } else {
            $("#player-add-modal .match-input").hide();
        }

        $('#player-add-modal select[name="league"]').empty();
        $.each(leagues, function (index, league) {
            const option = $('<option>', {
                value: league.id,
                text: league.name
            });
            $('#player-add-modal select[name="league"]').append(option);
        });

        $('#player-add-modal select[name="team_a"], #player-add-modal select[name="team_b"]').empty();
        $.each(teams, function (index, team) {
            const option = $('<option>', {
                value: team.id,
                text: team.name
            });
            $('#player-add-modal select[name="team_a"], #player-add-modal select[name="team_b"]').append(option);
        });
        */
        MicroModal.show("player-add-modal");
    });

    $("#players").on("click", ".player-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        const player = app.players.get(id);

        let games = app.games.games;
        $('#player-edit-modal select[name="game"]').empty();
        $.each(games, function (index, game) {
            const option = $('<option>', {
                value: game.id,
                text: game.name
            });
            $('#player-edit-modal select[name="game"]').append(option);
        });

        let theForm = $("#player-edit-modal form");
        theForm.find('input[name="id"]').val(player.id);
        theForm.find('input[name="home_studium"]').val(player.home_studium);
        theForm.find('input[name="home_city"]').val(player.home_city);
        theForm.find('input[name="name"]').val(player.name).prop("disabled", true);
        theForm.find('select[name="game"] option[value="' + player.game_id + '"] ').prop('selected', true);
        theForm.find('textarea[name="description"]').val(player.description);
        theForm.find('input[name="birthdate"]').val(player.birthdate);
        theForm.find("img").attr("src", player.photo);

        MicroModal.show("player-edit-modal");
    });

    $("#player-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#player-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#player-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.players.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("player-edit-modal");
        };
        editPlayer(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#players").on("click", ".player-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (player) {
            successFunction(["Player has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deletePlayer(id, successFn, errorFn);
    });
});

function getPlayers() {
    return new Promise(resolve => {
        console.log('getting players');
        axios
            .get("players")
            .then(response => {
                app.players.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(true);
            });
    });
}

function showPlayers() {
    $(".section").hide(250);
    getPlayers();
    $("#players").show(250);
}

function showPlayer(id) {
    console.log('showing player');
    $(".section").hide(250);

    axios
        .get("players/" + id)
        .then(response => {
            let player = response.data;
            console.log(Boolean(player.id));
            if (player.id) {
                $("#player .player-name").text(player.name + " | " + player.game.name);
                $("#player .player-description").text(player.description);
                $("#player").show(250);
                
                $.each(player.players, (index, player) => {
                    var outp = Handlebars.templates.player({
                        "player": player,
                        "permissions": {
                            "edit_permission": app.matches.edit_permission,
                            "delete_permission": app.matches.delete_permission
                        }
                    });
                    $("#player .players").append(outp);
                });

                $.each(player.home_matches, (index, match) => {
                    var outp = Handlebars.templates.match({
                        "match": match,
                        "permissions": {
                            "edit_permission": app.matches.edit_permission,
                            "delete_permission": app.matches.delete_permission
                        }
                    });
                    $("#player .players").append(outp);
                });

                $.each(player.away_matches, (index, match) => {
                    var outp = Handlebars.templates.match({
                        "match": match,
                        "permissions": {
                            "edit_permission": app.matches.edit_permission,
                            "delete_permission": app.matches.delete_permission
                        }
                    });
                    $("#player .players").append(outp);
                });
            } else {
                $("#player .player-name").text("Player not found!");
                $("#player").show(250);
            }
        })
        .catch(function (error) {
            console.log(error);
        }).then(() => {

        });
}

function createPlayer(player, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "players",
            data: player,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deletePlayer(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("players/" + id)
        .then(response => {
            let player = app.players.delete(id);
            successFn(player);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editPlayer(player, successFn, errorFn) {
    const data = player;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "players/" + player.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                //errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getPlayer(id) {}
