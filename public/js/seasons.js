$("document").ready(function () {
    $("#season-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#season-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (season) {
            app.seasons.add(season);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text("New Season succesfully registered")
            );
            MicroModal.close("season-add-modal");
        };
        createSeason(
            new FormData($("#season-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-season").on("click", function () {
        console.log('adding season');
        MicroModal.show("season-add-modal");
    });

    $("#seasons").on("click", ".season-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        console.log(id);
        const season = app.seasons.get(id);
        console.log(season);

        let theForm = $("#season-edit-modal form");
        theForm.find('input[name="id"]').val(season.id);
        theForm.find('input[name="name"]').val(season.name).prop("disabled", true);
        theForm.find('select option[value="' + season.season.id + '"] ').prop('selected', true);
        theForm.find('textarea[name="description"]').val(season.description);
        theForm.find("img").attr("src", season.photo);

        MicroModal.show("season-edit-modal");
    });

    $("#season-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#season-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#season-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.seasons.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("season-edit-modal");
        };
        editSeason(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#seasons").on("click", ".season-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (season) {
            successFunction(["Season has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteSeason(id, successFn, errorFn);
    });
});

function getSeasons() {
    return new Promise(resolve => {
        console.log('getting seasons');
        axios
            .get("seasons")
            .then(response => {
                app.seasons.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(app.seasons.seasons);
            });
    });
}

function showSeasons() {
    $(".section").hide(250);
    getSeasons();
    $("#seasons").show(250);
}

function showSeason(id) {
    console.log('showing season');
    $(".section").hide(250);

    axios
        .get("seasons/" + id)
        .then(response => {
            let season = response.data.season;
            console.log(Boolean(season.id));
            if (season.id) {
                $("#season .season-name").text(season.league.name);
                $("#season .season-description").text(season.start_at + " - " + season.end_at);

                $("#season").show(250);
                $("#season .league-table").empty();
                $("#season .matches").empty();

                if (response.data.season_table.length) {
                    $.each(response.data.season_table, (index, team) => {
                        var outp = Handlebars.templates.match({
                            "match": match,
                            "permissions": {
                                "edit_permission": app.matches.edit_permission,
                                "delete_permission": app.matches.delete_permission
                            }
                        });
                        $("#league .leagues").append(outp);
                    });
                } else {
                    $("#season .league-table").text('No Teams/Matches');
                }

                if (season.matches.length) {
                    $.each(season.matches, (index, match) => {
                        var outp = Handlebars.templates.match({
                            "match": match,
                            "permissions": {
                                "edit_permission": app.matches.edit_permission,
                                "delete_permission": app.matches.delete_permission
                            }
                        });
                        $("#season .matches").append(outp);
                    });
                } else {
                    $("#season .matches").text('No Teams/Matches');
                }

            } else {
                $("#season .season-name").text("Season not found!");
                $("#season").show(250);
            }
        })
        .catch(function (error) {
            console.log(error);
        }).then(() => {

        });
}



function createSeason(season, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "seasons",
            data: season,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deleteSeason(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("seasons/" + id)
        .then(response => {
            let season = app.seasons.delete(id);
            successFn(season);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editSeason(season, successFn, errorFn) {
    const data = season;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "seasons/" + season.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getSeason(id) {}
