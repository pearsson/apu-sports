function validateImage(input, size, maxwidth, maxheight) {
    return new Promise(resolve => {
        var errors = {
            size: false,
            dimensions: false,
            validImage: false
        };

        if (typeof input.files[0] !== "undefined") {
            var max = size,
                mySize = input.files[0].size / 1000;
            if (mySize > max) {
                errors.size =
                    "file size must be less than " + max / 1000 + " Mb";
            }

            var _URL = window.URL || window.webkitURL;

            var file = input.files[0];

            var img = new Image();
            var imgwidth = 0;
            var imgheight = 0;

            img.src = _URL.createObjectURL(file);
            img.onload = function() {
                imgwidth = this.width;
                imgheight = this.height;

                if (imgwidth > maxwidth || imgheight > maxheight) {
                    errors.dimensions =
                        "Image size must be smaller/equal to" +
                        maxwidth +
                        "X" +
                        maxheight;
                }

                resolve(errors);
            };
            img.onerror = function() {
                errors.validImage = file.type + " : Not a valid image file";
                resolve(errors);
            };
        } else {
            resolve(errors);
        }
    });
}

function selectImage(event) {
    event.target
        .closest(".image-changer-wrapper")
        .querySelectorAll(".new-img-upload")[0]
        .click();
}

function previewImage(event) {
    if (!event) return;

    var selectedFile = event.target.files[0];

    var imageContainer = event.target.parentNode.querySelectorAll("img")[0];

    File.prototype.convertToBase64 = function(callback) {
        var reader = new FileReader();
        reader.onloadend = function(e) {
            callback(e.target.result, e.target.error);
        };
        reader.readAsDataURL(this);
    };

    selectedFile.convertToBase64(function(base64) {
        imageContainer.setAttribute("src", base64);
    });
}

var imagePickers = document.querySelectorAll(".image-changer-wrapper i");

Array.prototype.forEach.call(imagePickers, function(el, i) {
    el.addEventListener("click", selectImage);
    el.parentNode.parentNode
        .querySelector("input")
        .addEventListener("change", previewImage);
});
