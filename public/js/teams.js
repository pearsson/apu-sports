$("document").ready(function () {
    $("#team-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#team-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.teams.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("team-add-modal");
        };
        createTeam(
            new FormData($("#team-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-team").on("click", function () {
        clearAlerts();

        let games = app.games.games;
        if (games.length < 1) {
            errorFunction(['Register at lease 1 sport to add teams.']);
            return
        }

        $('#team-add-modal select[name="game"]').empty();
        $.each(games, function (index, game) {
            const option = $('<option>', {
                value: game.id,
                text: game.name
            });
            $('#team-add-modal select[name="game"]').append(option);
        });
        MicroModal.show("team-add-modal");
    });

    $("#teams").on("click", ".team-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        const team = app.teams.get(id);

        let games = app.games.games;
        $('#team-edit-modal select[name="game"]').empty();
        $.each(games, function (index, game) {
            const option = $('<option>', {
                value: game.id,
                text: game.name
            });
            $('#team-edit-modal select[name="game"]').append(option);
        });

        let theForm = $("#team-edit-modal form");
        theForm.find('input[name="id"]').val(team.id);
        theForm.find('input[name="home_studium"]').val(team.home_studium);
        theForm.find('input[name="home_city"]').val(team.home_city);
        theForm.find('input[name="name"]').val(team.name).prop("disabled", true);
        theForm.find('select[name="game"] option[value="' + team.game_id + '"] ').prop('selected', true);
        theForm.find('textarea[name="description"]').val(team.description);
        theForm.find('input[name="birthdate"]').val(team.birthdate);
        theForm.find("img").attr("src", team.photo);

        MicroModal.show("team-edit-modal");
    });

    $("#team-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#team-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#team-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.teams.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("team-edit-modal");
        };
        editTeam(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#teams").on("click", ".team-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (team) {
            successFunction(["Team has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteTeam(id, successFn, errorFn);
    });
});

function getTeams() {
    return new Promise(resolve => {
        console.log('getting teams');
        axios
            .get("teams")
            .then(response => {
                app.teams.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(true);
            });
    });
}

function showTeams() {
    $(".section").hide(250);
    getTeams();
    $(".section").hide(250);
    $("#teams").show(250);
}

function showTeam(id) {
    console.log('showing team');
    $(".section").hide(250);

    axios
        .get("teams/" + id)
        .then(response => {
            let team = response.data;
            console.log(Boolean(team.id));
            if (team.id) {
                $("#team .team-name").text(team.name + " | " + team.game.name);
                $("#team .team-description").text(team.description);
                $("#team").show(250);

                $("#player-add-modal input[name='team']").val(team.id);

                $("#team .players").empty();
                if (team.players.length > 0) {
                    $.each(team.players, (index, player) => {
                        var outp = Handlebars.templates.player({
                            "player": player,
                            "permissions": {
                                "edit_permission": app.players.edit_permission,
                                "delete_permission": app.players.delete_permission
                            }
                        });
                        $("#team .players").append(outp);
                    });
                } else {
                    $("#team .players").text('Team has no players.');
                }

                $("#team .matches").empty();
                $.each(team.home_matches, (index, match) => {
                    var outp = Handlebars.templates.match({
                        "match": match,
                        "permissions": {
                            "edit_permission": app.matches.edit_permission,
                            "delete_permission": app.matches.delete_permission
                        }
                    });
                    $("#team .teams").append(outp);
                });

                $.each(team.away_matches, (index, match) => {
                    var outp = Handlebars.templates.match({
                        "match": match,
                        "permissions": {
                            "edit_permission": app.matches.edit_permission,
                            "delete_permission": app.matches.delete_permission
                        }
                    });
                    $("#team .teams").append(outp);
                });
            } else {
                $("#team .team-name").text("Team not found!");
                $("#team").show(250);
            }
        })
        .catch(function (error) {
            console.log(error);
        }).then(() => {

        });
}

function createTeam(team, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "teams",
            data: team,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deleteTeam(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("teams/" + id)
        .then(response => {
            let team = app.teams.delete(id);
            successFn(team);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editTeam(team, successFn, errorFn) {
    const data = team;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "teams/" + team.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getTeam(id) {}
