function logout(data) {
    axios({
            method: "post",
            url: "logout",
            data: data,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            window.location.href = "/";
        })
        .catch(function (response) {
            console.log(response);
        });
}
