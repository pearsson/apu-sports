$("document").ready(function () {
    $("#home-add-modal select[name='type']").on("change", function () {
        let type = $(this).val();
        console.log(type);
        if (type == "match") {
            $("#home-add-modal .match-input").show();
        } else {
            $("#home-add-modal .match-input").hide();
        }
    });

    $("#home-add-modal select[name='league']").on("change", function () {
        $("#home-edit-modal .error-alert .errors").empty();
        let league_id = $(this).val();
        let seasons = app.leagues.get(league_id);
        console.log(seasons);
        if (seasons.length > 0) {
            $('#home-add-modal select[name="season"]').empty();
            $.each(seasons, function (index, season) {
                const option = $("<option>", {
                    value: season.id,
                    text: season.name
                });
                $('#home-add-modal select[name="season"]').append(option);
            });
        } else {
            $("#home-edit-modal .error-alert .errors").append(
                $("<p></p>").text("This league has no season(s)")
            );
        }
    });

    $("#home-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#home-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.homes.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("home-add-modal");
        };
        createHome(
            new FormData($("#home-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-home").on("click", function () {
        clearAlerts();

        let teams = app.teams.teams;
        if (teams.length < 2) {
            errorFunction(["There are less than 2 registered teams"]);
            return;
        }

        let leagues = app.leagues.leagues;
        if (leagues.length < 1) {
            errorFunction(["No registered sports leagues"]);
            return;
        }

        let type = $("#home-add-modal select[name='type']").val();
        console.log(type);
        if (type == "match") {
            $("#home-add-modal .match-input").show();
        } else {
            $("#home-add-modal .match-input").hide();
        }

        $('#home-add-modal select[name="league"]').empty();
        $.each(leagues, function (index, league) {
            const option = $("<option>", {
                value: league.id,
                text: league.name
            });
            $('#home-add-modal select[name="league"]').append(option);
        });

        $(
            '#home-add-modal select[name="team_a"], #home-add-modal select[name="team_b"]'
        ).empty();
        $.each(teams, function (index, team) {
            const option = $("<option>", {
                value: team.id,
                text: team.name
            });
            $(
                '#home-add-modal select[name="team_a"], #home-add-modal select[name="team_b"]'
            ).append(option);
        });
        MicroModal.show("home-add-modal");
    });

    $("#homes").on("click", ".home-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        const home = app.homes.get(id);

        let games = app.games.games;
        $('#home-edit-modal select[name="game"]').empty();
        $.each(games, function (index, game) {
            const option = $("<option>", {
                value: game.id,
                text: game.name
            });
            $('#home-edit-modal select[name="game"]').append(option);
        });

        let theForm = $("#home-edit-modal form");
        theForm.find('input[name="id"]').val(home.id);
        theForm.find('input[name="home_studium"]').val(home.home_studium);
        theForm.find('input[name="home_city"]').val(home.home_city);
        theForm
            .find('input[name="name"]')
            .val(home.name)
            .prop("disabled", true);
        theForm
            .find('select[name="game"] option[value="' + home.game_id + '"] ')
            .prop("selected", true);
        theForm.find('textarea[name="description"]').val(home.description);
        theForm.find('input[name="birthdate"]').val(home.birthdate);
        theForm.find("img").attr("src", home.photo);

        MicroModal.show("home-edit-modal");
    });

    $("#home-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#home-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#home-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.homes.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("home-edit-modal");
        };
        editHome(data, successFunction, errorFunction);
    });

    $("#homes").on("click", ".home-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (home) {
            successFunction(["Home has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteHome(id, successFn, errorFn);
    });
});

function getHomes() {
    return new Promise(resolve => {
        console.log("getting homes");
        axios
            .get("homes")
            .then(response => {
                app.homes.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(() => {
                resolve(true);
            });
    });
}

function showHome() {
    console.log("showing home");
    $(".section").hide(250);

    axios
        .get("leagues/tables")
        .then(response => {
            let tables = response.data;
            $.each(tables.leagues, (index, leagues) => {
                var outp = Handlebars.templates.player({
                    player: player,
                    permissions: {
                        edit_permission: app.matches.edit_permission,
                        delete_permission: app.matches.delete_permission
                    }
                });
                $("#home .players").append(outp);
            });
        })
        .catch(function (error) {
            console.log(error);
        })
        .then(() => {});
    $("#home").show(250);
}

function createHome(home, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "homes",
            data: home,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deleteHome(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("homes/" + id)
        .then(response => {
            let home = app.homes.delete(id);
            successFn(home);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editHome(home, successFn, errorFn) {
    const data = home;
    const config = {
        headers: {
            "content-type": "multipart/form-data"
        }
    };
    const url = "homes/" + home.get("id");

    axios
        .post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                //errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getHome(id) {}
