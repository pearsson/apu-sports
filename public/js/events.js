$("document").ready(function () {

    $("#event-add-modal select[name='type'], #event-edit-modal select[name='type']").on("change", function () {
        let type = $(this).val();

        if (type == "match") {

            let games = app.games.games;
            let teams = app.teams.teams;
            let leagues = app.leagues.leagues;

            if (games.length < 1) {
                errorFunction(['To create a match register atleast one game']);
                return
            }

            if (teams.length < 2) {
                errorFunction(['There are less than 2 registered teams']);
                return
            }

            if (leagues.length < 1) {
                errorFunction(['No registered sports leagues']);
                return
            }


            let theForm = $(this).closest("form");

            theForm.find('select[name="league"]').empty();
            $.each(leagues, function (index, league) {
                const option = $('<option>', {
                    value: league.id,
                    text: league.name
                });

                theForm.find('select[name="league"]').append(option);
            });

            let selectedLeagueId = theForm.find('select[name="league"]').val();
            let selectedLeague = app.leagues.get(selectedLeagueId);

            theForm.find('select[name="season"]').empty();

            if (selectedLeague && selectedLeague.seasons) {
                $.each(selectedLeague.seasons, function (index, season) {
                    const option = $('<option>', {
                        value: season.id,
                        text: season.start_at + " - " + season.end_at
                    });
                    theForm.find('select[name="season"]').append(option);
                });
            }

            theForm.find('select[name="team_a"], select[name="team_b"]').empty();
            $.each(teams, function (index, team) {
                const option = $('<option>', {
                    value: team.id,
                    text: team.name
                });
                theForm.find('select[name="team_a"], select[name="team_b"]').append(option);
            });

            theForm.find(".match-input").show();
        } else {
            theForm.find(".match-input").hide();
        }
    });

    $("#event-add-modal select[name='league'], #event-edit-modal select[name='league']").on("change", function () {
        $(".error-alert .errors").empty();
        let league_id = $(this).val();
        let seasons = app.leagues.get(league_id);
        if (seasons.length > 0) {
            theForm.find('select[name="season"]').empty();
            $.each(seasons, function (index, season) {
                const option = $('<option>', {
                    value: season.id,
                    text: season.name
                });
                theForm.find('select[name="season"]').append(option);
            });
        } else {
            $(this).closest(".error-alert .errors").append(
                $("<p></p>").text('This league has no season(s)')
            );
        }
    });

    $("#event-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#event-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.events.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("event-add-modal");
        };
        createEvent(
            new FormData($("#event-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-event").on("click", function () {
        clearAlerts();

        let type = $("#event-add-modal select[name='type']").val();
        console.log(type);
        if (type == "match") {
            $("#event-add-modal .match-input").show();
        } else {
            $("#event-add-modal .match-input").hide();
        }

        MicroModal.show("event-add-modal");
    });

    $("#events").on("click", ".event-card button.edit", function () {

        const id = $(this).data("id");

        const event = app.events.get(id);

        let theForm = $("#event-edit-modal form");

        if (event.type == "match") {

            let teams = app.teams.teams;
            let leagues = app.leagues.leagues;

            let theForm = $('#event-edit-modal form');

            theForm.find('select[name="league"]').empty();
            $.each(leagues, function (index, league) {
                const option = $('<option>', {
                    value: league.id,
                    text: league.name
                });

                theForm.find('select[name="league"]').append(option);
            });

            theForm.find('select[name="league"] option[value="' + event.match.league_id + '"]').prop('selected', true);

            let selectedLeagueId = theForm.find('select[name="league"]').val();
            let selectedLeague = app.leagues.get(selectedLeagueId);

            theForm.find('select[name="season"]').empty();
            if (selectedLeague && selectedLeague.seasons) {
                $.each(selectedLeague.seasons, function (index, season) {
                    const option = $('<option>', {
                        value: season.id,
                        text: season.start_at + " - " + season.end_at
                    });
                    theForm.find('select[name="season"]').append(option);
                });
            }

            theForm.find('select[name="season"] option[value="' + event.match.season_id + '"]').prop('selected', true);

            theForm.find('select[name="team_a"], select[name="team_b"]').empty();
            $.each(teams, function (index, team) {
                const option = $('<option>', {
                    value: team.id,
                    text: team.name
                });
                theForm.find('select[name="team_a"], select[name="team_b"]').append(option);
            });


            theForm.find('select[name="team_a"] option[value="' + event.match.team_a + '"]').prop('selected', true);
            theForm.find('select[name="team_b"] option[value="' + event.match.team_b + '"]').prop('selected', true);

            theForm.find(".match-input").show();
        } else {
            theForm.find(".match-input").hide();
        }
        theForm.find('input[name="id"]').val(event.id);
        theForm.find('input[name="name"]').val(event.name);
        theForm.find('input[name="venue"]').val(event.venue);
        theForm.find('input[name="start_at"]').val(event.start_at);
        theForm.find('input[name="end_at"]').val(event.end_at);
        theForm.find('select[name="type"] option[value="' + event.type + '"]').prop('selected', true);

        theForm.find('textarea[name="description"]').val(event.description);
        theForm.find("img").attr("src", event.photo);

        MicroModal.show("event-edit-modal");
    });

    $("#event-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#event-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#event-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.events.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("event-edit-modal");
        };
        editEvent(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#events").on("click", ".event-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (event) {
            successFunction(["Event has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteEvent(id, successFn, errorFn);
    });

    $("#btn-add-photo").on("click", function () {
        clearAlerts();
        $('#event-image-upload-form input[type="file"]').click();
    });

    $('#event-image-upload-form input[type="file"]').on('change', function () {
        clearAlerts();
        let data = new FormData($("#event-image-upload-form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $(".body-alert.error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (event) {
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text("Images uploaded succesfully")
            );

            console.log(event);
            Array.prototype.forEach.call(event.photos, function (photo, i) {
                var img = $('<img />', {
                    id: photo.id,
                    src: photo.photo,
                    alt: 'Alternative Text',
                    class: 'w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 h-auto'
                });
                img.appendTo($('#event .images'));
            });

        };
        uploadEventImages(
            data,
            errorFunction,
            successFunction
        );
    });
});

function getEvents() {
    return new Promise(resolve => {
        console.log('getting events');
        axios
            .get("events")
            .then(response => {
                app.events.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(true);
            });
    });
}

function showEvents() {
    $(".section").hide(250);
    getEvents();
    $("#events").show(250);
}

function showEvent(id) {
    console.log('showing event');
    $(".section").hide(250);

    axios
        .get("events/" + id)
        .then(response => {
            let event = response.data;

            if (event.id) {
                $('#event-image-upload-form input[name="event"]').val(event.id);
                $("#event .event-name").text(event.name);
                $("#event .event-type").text(event.type);
                $("#event .event-venue").text(event.venue);
                $("#event .event-time").text(event.start_at + " to " + event.end_at);
                $("#event .event-description").text(event.description);

                if (event.match) {
                    var outp = Handlebars.templates.match({
                        "event": event,
                        "permissions": {
                            "edit_permission": app.events.edit_permission,
                            "delete_permission": app.events.delete_permission
                        }
                    });
                    $("#event .match-container .match").empty().append(outp);
                    $("#event .match-container").removeClass('hidden');
                } else {
                    $("#event .match-container").addClass('hidden');
                }

                $("#event").show(250);

                $.each(event.photos, (index, photo) => {
                    const image = $('<img />', {
                        id: photo.id,
                        src: photo.photo,
                        alt: 'Event Image',
                        class: 'w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 h-auto'
                    });
                    $("#event .images").append(image);
                });
            } else {
                $("#event .event-name").text("Event not found!");
                $("#event").show(250);
            }
        })
        .catch(function (error) {
            console.log(error);
        }).then(() => {

        });
}

function createEvent(event, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "events",
            data: event,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function uploadEventImages(event, errorFunction, successFunction) {
    axios({
            method: "post",
            url: `events/${event.get('id')}/images`,
            data: event,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deleteEvent(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("events/" + id)
        .then(response => {
            let event = app.events.delete(id);
            successFn(event);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editEvent(event, successFn, errorFn) {
    const data = event;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "events/" + event.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                //errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getEvent(id) {}
