(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['event'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-1/2 lg:flex my-3 event-card\">\n    <div class=\"h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\"\n        style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n    </div>\n    <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n        <div class=\"\">\n            <div class=\"text-black font-bold text-sm\">\n                <table>\n                    <tr>\n                        <td>Name : </td>\n                        <td><a href=\"/#/events/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></td>\n                    </tr>\n                    <tr>\n                        <td>From : </td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.start_at : stack1), depth0))
    + "</td>\n                    </tr>\n                    <tr>\n                        <td>To : </td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.end_at : stack1), depth0))
    + "</td>\n                    </tr>\n                    <tr>\n                        <td>At : </td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.venue : stack1), depth0))
    + "</td>\n                    </tr>\n                    <tr>\n                        <td>Event Type : </td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.type : stack1), depth0))
    + "</td>\n                    </tr>\n                </table>\n            </div>\n            <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n            <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['game'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-full md:w-1/2 lg:w-1/3 my-3 px-2 game-card\">\n    <div class=\"h-32 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\"\n        style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n    </div>\n    <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n        <div class=\"\">\n            <div class=\"text-black font-bold text-sm\">\n                <table>\n                    <tr>\n                        <td colspan=\"2\"><a href=\"/#/games/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.leagues : stack1)) != null ? stack1.length : stack1), depth0))
    + " Leagues</td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.teams : stack1)) != null ? stack1.length : stack1), depth0))
    + " Teams</td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.upcoming_matches : stack1)) != null ? stack1.length : stack1), depth0))
    + " Upcoming events</td>\n                    </tr>\n                </table>\n            </div>\n            <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n            <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['games'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\" data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"  data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-full md:w-1/2 lg:w-1/3 my-3 px-2 game-card\">\n  <div class=\"h-32 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\" style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n  </div>\n  <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n    <div class=\"\">\n      <div class=\"text-black font-bold text-sm\">\n          <table>\n              <tr>\n                  <td colspan=\"2\"><a href=\"/#/games/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.leagues : stack1)) != null ? stack1.length : stack1), depth0))
    + " Leagues</td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.teams : stack1)) != null ? stack1.length : stack1), depth0))
    + " Teams</td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.upcoming_matches : stack1)) != null ? stack1.length : stack1), depth0))
    + " Upcoming events</td>\n              </tr>\n          </table>\n      </div>\n      <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n          <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    </div>\n  </div>\n</div>";
},"useData":true});
templates['league'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-full md:w-1/2 lg:w-1/3 my-3 px-2 league-card\">\n    <div class=\"h-32 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\"\n        style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n    </div>\n    <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n        <div class=\"\">\n            <div class=\"text-black font-bold text-sm\">\n                <table>\n                    <tr>\n                        <td><a href=\"/#/leagues/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></td>\n                    </tr>\n                    <tr>\n                        <td>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.game : stack1)) != null ? stack1.name : stack1), depth0))
    + "</td>\n                    </tr>\n                </table>\n            </div>\n            <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.league : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n            <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['leagueTable'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<div class=\"w-full league-table-card\">\n    <table>\n        <tr>\n            <td colspan=\"2\"><a href=\"/#/games/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></td>\n        </tr>\n        <tr>\n            <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.leagues : stack1)) != null ? stack1.length : stack1), depth0))
    + " Leagues</td>\n        </tr>\n        <tr>\n            <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.teams : stack1)) != null ? stack1.length : stack1), depth0))
    + " Teams</td>\n        </tr>\n        <tr>\n            <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.game : depth0)) != null ? stack1.upcoming_matches : stack1)) != null ? stack1.length : stack1), depth0))
    + " Upcoming events</td>\n        </tr>\n    </table>\n</div>";
},"useData":true});
templates['match'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                    <tr>\n                        <td colspan=\"2\">\n                            <span>\n                                <span class=\"block\">\n                                    "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.team_a : stack1)) != null ? stack1.name : stack1), depth0))
    + "\n                                </span>\n                                <span class=\"block\">\n                                    "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.results : stack1)) != null ? stack1.team_a_score : stack1), depth0))
    + "\n                                </span>\n                            </span>\n                            VS\n                            <span>\n                                <span class=\"block\">\n                                    "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.team_b : stack1)) != null ? stack1.name : stack1), depth0))
    + "\n                                </span>\n                                <span class=\"block\">\n                                    "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.results : stack1)) != null ? stack1.team_b_score : stack1), depth0))
    + "\n                                </span>\n                            </span>\n                        </td>\n                    </tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                    <tr>\n                        <td colspan=\"2\">"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.team_a : stack1)) != null ? stack1.name : stack1), depth0))
    + " VS "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.team_b : stack1)) != null ? stack1.name : stack1), depth0))
    + "</td>\n                    </tr>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                <button\n                    class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"\n                    data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-full my-3 match-card\">\n    <div class=\"h-32 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\"\n        style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n    </div>\n    <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n        <div class=\"\">\n            <div class=\"text-black  text-base\">\n                <table>\n                    <tr>\n                        <td>Event Title : </td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.name : stack1), depth0))
    + "</td>\n                    </tr>\n                    <tr>\n                        <td>Venue</td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.venue : stack1), depth0))
    + "</td>\n                    </tr>\n                    <tr>\n                        <td>Time</td>\n                        <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.start_at : stack1), depth0))
    + "</td>\n                    </tr>\n                    <tr>\n                        <td>League</td>\n                        <td>"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.league : stack1)) != null ? stack1.name : stack1), depth0))
    + "</td>\n                    </tr>\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.results : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "                </table>\n            </div>\n            <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.event : depth0)) != null ? stack1.match : stack1)) != null ? stack1.description : stack1), depth0))
    + "</p>\n            <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['player'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\" data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"  data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-1/2 my-3 px-2 player-card\">\n    <div class=\"h-32 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\"\n        style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n    </div>\n  <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n    <div class=\"\">\n      <div class=\"text-black font-bold text-sm\">\n          <table>\n              <tr>\n                  <td colspan=\"2\" >"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.user : stack1)) != null ? stack1.name : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">Since "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.contract_start : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">Contract ending on "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.contract_end : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Position</td>\n                  <td> : "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.position : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Number</td>\n                  <td> : "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.number : stack1), depth0))
    + "</td>\n              </tr>\n          </table>\n      </div>\n      <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.player : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n          <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    </div>\n  </div>\n</div>";
},"useData":true});
templates['season'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=container.lambda;

  return "<div class=\"w-full my-3 season-card\">\n  <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n    <div class=\"\">\n      <div class=\"text-black font-bold text-sm\">\n          <table>\n              <tr>\n                  <td>League : </td>\n                  <td>"
    + alias1(((helper = (helper = helpers.league || (depth0 != null ? depth0.league : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"league","hash":{},"data":data}) : helper)))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Begins : </td>\n                  <td>"
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.season : depth0)) != null ? stack1.start_at : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Ends : </td>\n                  <td>"
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.season : depth0)) != null ? stack1.end_at : stack1), depth0))
    + "</td>\n              </tr>\n               <tr>\n                  <td colspan=\"2\"><a href=\"/#/seasons/"
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.season : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">View Matches</a></td>\n              </tr>\n          </table>\n      </div>\n    </div>\n  </div>\n</div>";
},"useData":true});
templates['team'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\" data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"  data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-full md:w-1/2 lg:w-1/3 my-3 px-2 team-card\">\n    <div class=\"h-32 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\"\n        style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n    </div>\n  <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n    <div class=\"\">\n      <div class=\"text-black font-bold text-sm\">\n          <table>\n              <tr>\n                  <td colspan=\"2\"><a href=\"/#/teams/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></td>\n              </tr>\n              <tr>\n                  <td>Founded</td>\n                  <td> : "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.birthdate : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Home Studium</td>\n                  <td> : "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.home_studium : stack1), depth0))
    + "</td>\n              </tr>\n          </table>\n      </div>\n      <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.team : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n          <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    </div>\n  </div>\n</div>";
},"useData":true});
templates['user'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded edit\" data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">edit</button>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "              <button class=\"shadow bg-purple hover:bg-purple-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 mx-3 rounded delete\"  data-id=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.id : stack1), depth0))
    + "\">delete</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"w-1/2 lg:flex my-3 member-card\">\n  <div class=\"h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden\" style=\"background-image: url('"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.photo : stack1), depth0))
    + "')\" title=\"profile image\">\n  </div>\n  <div class=\"bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal\">\n    <div class=\"\">\n      <div class=\"text-black font-bold text-sm\">\n          <table>\n              <tr>\n                  <td>Name : </td>\n                  <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.name : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Email : </td>\n                  <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.email : stack1), depth0))
    + "</td>\n              </tr>\n              <tr>\n                  <td>Phone : </td>\n                  <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.phone : stack1), depth0))
    + "</td>\n              </tr>\n          </table>\n      </div>\n      <p class=\"text-grey-darker text-base\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n          <div class=\"flex items-center\">\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.edit_permission : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.permissions : depth0)) != null ? stack1.delete_permission : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    </div>\n  </div>\n</div>";
},"useData":true});
})();