var member = {
    name: Legalize.string().minLength(1).maxLength(30).required(),
    email: Legalize.string().minLength(1).maxLength(30).required(),
    height: Legalize.number().min(1).required(),
    weight: Legalize.number().min(1).required(),

   //'m/d/Y', $_POST["birthdate"]);

    phone: Legalize.string().minLength(1).maxLength(30).required(),
    password: Legalize.string().minLength(3).required(),
};