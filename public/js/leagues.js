$("document").ready(function () {
    $("#league-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#league-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.leagues.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("league-add-modal");
        };
        createLeague(
            new FormData($("#league-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-league").on("click", function () {
        clearAlerts();
        let games = app.games.games;
        $('#league-add-modal select[name="game"]').empty();
        $.each(games, function (index, game) {
            const option = $('<option>', {
                value: game.id,
                text: game.name
            });
            $('#league-add-modal select[name="game"]').append(option);
        });
        MicroModal.show("league-add-modal");
    });

    $("#leagues").on("click", ".league-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        const league = app.leagues.get(id);

        let games = app.games.games;
        $('#league-add-modal select[name="game"]').empty();
        $.each(games, function (index, game) {
            const option = $('<option>', {
                value: game.id,
                text: game.name
            });
            $('#league-edit-modal select[name="game"]').append(option);
        });

        let theForm = $("#league-edit-modal form");
        theForm.find('input[name="id"]').val(league.id);
        theForm.find('input[name="name"]').val(league.name);
        theForm.find('select option[value="' + league.game_id + '"] ').prop('selected', true);
        theForm.find('textarea[name="description"]').val(league.description);
        theForm.find("img").attr("src", league.photo);

        MicroModal.show("league-edit-modal");
    });

    $("#league-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#league-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#league-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.leagues.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("league-edit-modal");
        };
        editLeague(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#leagues").on("click", ".league-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (league) {
            successFunction(["League has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteLeague(id, successFn, errorFn);
    });

    $('#leagues .search').on('keyup', function () {
        let query = $(this).val();
        app.leagues.search(query);
    });
});

function getLeagues() {
    return new Promise(resolve => {
        console.log('getting leagues');
        axios
            .get("leagues")
            .then(response => {
                app.leagues.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(true);
            });
    });
}

function showLeagues() {
    $(".section").hide(250);
    getLeagues();
    $(".section").hide(250);
    $("#leagues").show(250);
}

function showLeague(id) {
    console.log('showing league');
    $(".section").hide(250);

    axios
        .get("leagues/" + id)
        .then(response => {
            let league = response.data;
            console.log(Boolean(league.id));
            if (league.id) {
                $("#league .league-name").text(league.name+ " | "+league.game.name);
                $("#league .league-description").text(league.description);

                $("#league #btn-add-season").data('id',league.id);
                $("#season-add-modal input[name='league']").val(league.id);
                $("#league").show(250);
                $("#league .leagues").empty();
                $.each(league.matches, (index, match) => {
                    var outp = Handlebars.templates.match({
                        "match": match,
                        "permissions": {
                            "edit_permission": app.matches.edit_permission,
                            "delete_permission": app.matches.delete_permission
                        }
                    });
                    $("#league .leagues").append(outp);
                });

                $("#league .teams").empty();
                $.each(league.teams, (index, team) => {
                    var outp = Handlebars.templates.team({
                        "team": team,
                        "permissions": {
                            "edit_permission": app.teams.edit_permission,
                            "delete_permission": app.teams.delete_permission
                        }
                    });
                    $("#league .teams").append(outp);
                });

                $("#league .seasons").empty();
                $.each(league.seasons, (index, season) => {
                    var outp = Handlebars.templates.season({
                        "season": season,
                        "league":league.name,
                        "permissions": {
                            "edit_permission": app.seasons.edit_permission,
                            "delete_permission": app.seasons.delete_permission
                        }
                    });
                    $("#league .seasons").append(outp);
                });
            } else {
                $("#league .league-name").text("League not found!");
                $("#league").show(250);
            }
        })
        .catch(function (error) {
            console.log(error);
        }).then(() => {

        });
}

function createLeague(league, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "leagues",
            data: league,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deleteLeague(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("leagues/" + id)
        .then(response => {
            let league = app.leagues.delete(id);
            successFn(league);
        })
        .catch(function (error) {
            if (error.response) {
                errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function editLeague(league, successFn, errorFn) {
    const data = league;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "leagues/" + league.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getLeague(id) {}
