$("document").ready(function () {
    $("#game-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#game-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.games.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("game-add-modal");
        };
        createGame(
            new FormData($("#game-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-game").on("click", function () {
        clearAlerts();
        MicroModal.show("game-add-modal");
    });

    $("#games").on("click", ".game-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        const game = app.games.get(id);

        let theForm = $("#game-edit-modal form");
        theForm.find('input[name="id"]').val(game.id);
        theForm.find('input[name="name"]').val(game.name);
        theForm.find('textarea[name="description"]').val(game.description);
        theForm.find("img").attr("src", game.photo);

        MicroModal.show("game-edit-modal");
    });

    $("#game-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#game-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#game-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.games.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("game-edit-modal");
        };
        editGame(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#games").on("click", ".game-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (game) {
            successFunction(["Game has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteGame(id, successFn, errorFn);
    });

    $('#games .search').on('keyup', function () {
        let query = $(this).val();
        app.games.search(query);
    });
});

function getGames() {
    return new Promise(resolve => {
        console.log('getting games');
        axios
            .get("games")
            .then(response => {
                app.games.set(response.data);
                console.log('getting done fetching games');
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(app.games.games);
            });
    });
}

function showGames() {
    $(".section").hide(250);
    $("#games").show(250);
}

function showGame(id) {
    console.log('showing game');
    $(".section").hide(250);

    axios
        .get("games/" + id)
        .then(response => {
            let game = response.data;
            console.log(Boolean(game.id));
            if (game.id) {
                $("#game .game-name").text(game.name);
                $("#game .game-description").text(game.description);
                $("#game").show(250);
                $.each(game.leagues, (index, league) => {
                    var outp = Handlebars.templates.league({
                        "league": league,
                        "permissions": {
                            "edit_permission": app.leagues.edit_permission,
                            "delete_permission": app.leagues.delete_permission
                        }
                    });
                    $("#game .leagues").append(outp);
                });

                $.each(game.teams, (index, team) => {
                    var outp = Handlebars.templates.team({
                        "team": team,
                        "permissions": {
                            "edit_permission": app.teams.edit_permission,
                            "delete_permission": app.teams.delete_permission
                        }
                    });
                    $("#game .teams").append(outp);
                });

                $.each(game.upcoming_matches, (index, match) => {
                    var outp = Handlebars.templates.match({
                        "match": match,
                        "permissions": {
                            "edit_permission": app.upcoming_matches.edit_permission,
                            "delete_permission": app.upcoming_matches.delete_permission
                        }
                    });
                    $("#game .upcoming").append(outp);
                });
            } else {
                $("#game .game-name").text("Game not found!");
                $("#game").show(250);
            }
        })
        .catch(function (error) {
            console.log(error);
        }).then(() => {

        });
}

function createGame(game, errorFunction, successFunction) {
    return new Promise(resolve => {
        console.log('creating game');
        axios({
                method: "post",
                url: "games",
                data: game,
                config: {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            })
            .then(response => {
                successFunction(response.data);
                console.log('done creating game');
            })
            .catch(function (error) {
                if (error.response) {
                    errorFunction(error.response.data.errors);
                } else if (error.request) {
                    errorFunction([{
                        error: "This request could not be completed now."
                    }]);
                    console.log(error.request);
                } else {
                    errorFunction([{
                        error: error.message
                    }]);
                    console.log("Error", error.message);
                }
            }).then(() => {
                resolve(true);
            });
    });
}

function deleteGame(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("games/" + id)
        .then(response => {
            let game = app.games.delete(id);
            successFn(game);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editGame(game, successFn, errorFn) {
    const data = game;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "games/" + game.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getGame(id) {}
