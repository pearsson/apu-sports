$("document").ready(function () {
    $("#member-add-modal .btn-save").on("click", function () {
        clearAlerts();
        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (
                error,
                i
            ) {
                $("#member-add-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.members.add(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully registered")
            );
            MicroModal.close("member-add-modal");
        };
        createMember(
            new FormData($("#member-add-modal form")[0]),
            errorFunction,
            successFunction
        );
    });

    $("#btn-add-member").on("click", function () {
        clearAlerts();
        MicroModal.show("member-add-modal");
    });

    $("#members").on("click", ".member-card button.edit", function () {
        clearAlerts();
        const id = $(this).data("id");
        console.log(id);
        const member = app.members.get(id);
        console.log(member);

        let theForm = $("#member-edit-modal form");
        theForm.find('input[name="id"]').val(member.id);
        theForm.find('input[name="name"]').val(member.name).prop("disabled", true);
        theForm.find('input[name="email"]').val(member.email).prop("disabled", true);
        theForm.find('input[name="birthdate"]').val(member.birthdate);
        theForm.find('input[name="phone"]').val(member.phone);
        theForm.find('input[name="weight"]').val(member.weight);
        theForm.find('input[name="height"]').val(member.height);
        theForm.find('textarea[name="description"]').val(member.description);
        theForm.find("img").attr("src", member.photo);

        MicroModal.show("member-edit-modal");
    });

    $("#member-edit-modal .btn-save").on("click", function () {
        clearAlerts();
        let data = new FormData($("#member-edit-modal form")[0]);

        let errorFunction = function (errors) {
            Array.prototype.forEach.call(Object.values(errors), function (error, i) {
                $("#member-edit-modal .error-alert .errors").append(
                    $("<p></p>").text(error)
                );
            });
        };

        let successFunction = function (user) {
            app.members.edit(user);
            $(".body-alert.success-alert .errors").append(
                $("<p></p>").text(user.name + " succesfully edited")
            );
            MicroModal.close("member-edit-modal");
        };
        editMember(
            data,
            successFunction,
            errorFunction
        );
    });

    $("#members").on("click", ".member-card button.delete", function () {
        const id = $(this).data("id");

        let successFn = function (member) {
            successFunction(["Member has been deleted successfully"]);
        };

        let errorFn = function (messages) {
            errorFunction(messages);
        };

        deleteMember(id, successFn, errorFn);
    });

    $('#members .search').on('keyup', function () {
        let query = $(this).val();
        app.members.search(query);
    });
});

function getMembers() {
    return new Promise(resolve => {
        console.log('creating member');
        axios
            .get("members")
            .then(response => {
                app.members.set(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }).then(() => {
                resolve(true);
            });
    });
}

function showMembers() {
    $(".section").hide(250);
    $("#members").show(250);
}

function createMember(member, errorFunction, successFunction) {
    axios({
            method: "post",
            url: "members",
            data: member,
            config: {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        })
        .then(response => {
            successFunction(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFunction(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFunction([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFunction([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function deleteMember(id, successFn, errorFn) {
    clearAlerts();
    axios
        .delete("members/" + id)
        .then(response => {
            let member = app.members.delete(id);
            successFn(member);
        })
        .catch(function (error) {
            const response = error.response;
            errorFn(Object.values(response.data.messages));
        });
}

function editMember(member, successFn, errorFn) {
    const data = member;
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const url = "members/" + member.get('id');

    axios.post(url, data, config)
        .then(response => {
            successFn(response.data);
        })
        .catch(function (error) {
            if (error.response) {
                errorFn(error.response.data.errors);
                console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);
            } else if (error.request) {
                errorFn([{
                    error: "This request could not be completed now."
                }]);
                console.log(error.request);
            } else {
                errorFn([{
                    error: error.message
                }]);
                console.log("Error", error.message);
            }
        });
}

function getMember(id) {}
